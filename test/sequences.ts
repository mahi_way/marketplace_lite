import { FunctionTree } from 'function-tree'
import { amazonin } from '../credentials' // meesho
// import * as AMAZONACTIONS from '../src/amazon'
import { downloadAsinProductInfo } from '../src/amazon/sequences' // validAmazonCredentials,
// import { fetchMeeshoOrdersSequence } from '../src/meesho/sequences'
// import { fetchFlipkartOrders } from '../src/flipkart/sequences';

const ft = new FunctionTree()

// const {
//   createAmazonAuthfetch,
//   requestReport$,
//   requestReportResult$,
//   getReport$
// } = AMAZONACTIONS

// describe('Meesho Fetch Orders Sequence', () => {
//   it('All Orders', async (done) => {
//     await ft.run([fetchMeeshoOrdersSequence], {
//       credentials: meesho.anchor
//     })
//     done()
//   })
// })

// describe('Flipkart Fetch Orders Sequence', () => {
//   it('All Orders', async (done) => {
//     await ft.run([fetchFlipkartOrders, function sub({ props }) {
//       props.orderItems$.subscribe({
//         next: console.log,
//         complete: done
//       })
//     }], {
//       credentials: flipkart.setu,
//       params: {
//         from: '2020-02-07T20:36:02.805+05:30',
//         to: '2020-02-19T20:36:02.809+05:30'
//       }
//     })
//     // done()
//   }, 3000000)
// });

// describe('Amazon Browsenode XML', () => {
//   it('Download Browse Node', async (done) => {
//     await ft.run([
//       createAmazonAuthfetch,
//       requestReport$,
//       requestReportResult$,
//       getReport$,
//       function subscribe({ props }: any) {
//         props.generatedReportId$.subscribe(val => {
//           console.log(val);
//           done();
//         })
//       }
//     ], {
//       credentials: amazonin.anchor,
//       requestReportParams: {
//         ReportType: '_GET_XML_BROWSE_TREE_DATA_'
//       }
//     })
//     .catch(err => {
//       console.log(err);
//     })
//     // done();
//   }, 300000)
// })

describe('Amazon', () => {
  // it('Validate amazon marketplace', done => {
  //   ft.run([
  //     validAmazonCredentials,
  //       ({ props }) => {
  //         console.log(props)
  //       }
  //   ], {
  //     credentials: amazonin.anchor
  //   })
  //   .then(() => done())
  //   .catch(err => done(err))
  // }, 200000)

  it('Amazon Product ASIN Info', done => {
    ft.run([
      downloadAsinProductInfo,
      function viewResult({ props }) {
        console.log(props)
        console.log('REQUESTING')
        return props.productInfo$.toPromise().then(info => {
          console.log('RESULTT')
          console.log(info)
        }).catch(done)
      }
    ], {
      credentials: amazonin.anchor,
      asin: 'B075FSBWQF',
      amount: 1000,
      type: 'MARKETPLACE'
    })
    .then(() => done())
    .catch(done)
  }, 200000)
})
