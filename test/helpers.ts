import { meesho } from '../credentials'
// import { fetchOrders as fetchMeeshoOrders, SlaStatus } from '../src/meesho/helpers'
import { fetchReturns as fetchMeeshoReturns } from '../src/meesho/helpers'
// import { getCombs } from '../utils'

describe('Meesho Helpers', () => {
  // describe('Meesho Orders', () => {
  //   it('Fetch Orders without filter', async (done) => {
  //     const obs = fetchMeeshoOrders({
  //       url: 'https://supplier.meeshosupply.com/api/v1/order/fetch/all',
  //       status: 0,
  //       cookie: meesho.anchor.cookies,
  //       supplierId: meesho.anchor.sellerId,
  //       identifier: meesho.anchor.marketplaceId
  //     })
  //     obs.subscribe({
  //       next: console.log,
  //       complete: done
  //     })
  //   })

  //   it('Fetch Orders with filter for status 0', async () => {

  //     const orderTypes: Array<string> = Object.keys(SlaStatus)

  //     const combinations = getCombs(0, 3, orderTypes)
  //     expect.assertions(combinations.length)
  //     // tslint:disable-next-line: await-promise
  //     for await (let combo of combinations) {
  //       const obs = fetchMeeshoOrders({
  //         url: 'https://supplier.meeshosupply.com/api/v1/order/fetch/all',
  //         status: 0,
  //         cookie: meesho.anchor.cookies,
  //         supplierId: meesho.anchor.sellerId,
  //         identifier: meesho.anchor.marketplaceId,
  //         filter: {
  //           order_date: {
  //             min: '2019-12-01',
  //             max: '2020-01-31'
  //           },
  //           sla_status: [SlaStatus[combo]]
  //         }
  //       })
  //       const result = await obs.toPromise()
  //       expect(result).toHaveProperty('data')
  //     }
  //   })

  //   it('Fetch Orders with filter for status 1', async () => {

  //     const orderTypes: Array<string> = Object.keys(SlaStatus)

  //     const combinations = getCombs(0, 3, orderTypes)
  //     expect.assertions(combinations.length)
  //     // tslint:disable-next-line: await-promise
  //     for await (let combo of combinations) {
  //       const obs = fetchMeeshoOrders({
  //         url: 'https://supplier.meeshosupply.com/api/v1/order/fetch/all',
  //         status: 0,
  //         cookie: meesho.anchor.cookies,
  //         supplierId: meesho.anchor.sellerId,
  //         identifier: meesho.anchor.marketplaceId,
  //         filter: {
  //           order_date: {
  //             min: '2019-12-01',
  //             max: '2020-01-31'
  //           },
  //           sla_status: [SlaStatus[combo]]
  //         }
  //       })
  //       const result = await obs.toPromise()
  //       expect(result).toHaveProperty('data')
  //     }
  //   })
  // })

  describe('Meesho returns', () => {
    it('Fetch Meesho Returns', (done) => {
      const obs = fetchMeeshoReturns({
        cookie: meesho.anchor.cookies,
        supplierId: meesho.anchor.sellerId,
        identifier: meesho.anchor.marketplaceId
      })

      obs.subscribe({
        next: console.log,
        complete: done
      })
    })
  })
})
