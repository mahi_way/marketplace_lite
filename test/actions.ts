import { FunctionTree } from 'function-tree'
// import { fetchMeeshoOrders } from '../src/meesho' // fetchMeeshoLoginCredentials, fetchMeeshoOrders, fetchMeeshoReturns, fetchMeeshoPenalties, fetchMeeshoInvoices, fetchMeeshoOutstandingPreviousPayments, fetchMeeshoPaymentDetails, fetchShippedOrdersFileUrl
import { createAmazonAuthfetch, fetchAmazonMyFeeEstimate$, fetchAmazonCompetitivePricing$, fetchAmazonProductCategories$, fetchAmazonMatchingProduct$, fetchAmazonLowestOfferListings$, fetchAmazonGetLowestPricedOffers$ } from '../src/amazon' //
import { amazonin } from '../credentials' // meesho

const FT = new FunctionTree()

// describe('Meesho Actions', () => {
  // describe('Meesho Login', () => {
//   it('Login Meesho action', async (done) => {
//     const result = await fetchMeeshoLoginCredentials({ props: { username: meesho.violina.username, password: meesho.violina.password } })
//     console.log(result)
//     done()
//   }, 20000)
// })

// describe('Fetch Orders', () => {
//   it('Pending Orders', async (done) => {
//     const result = fetchMeeshoOrders('pendingOrder$', 'https://supplier.meeshosupply.com/api/v1/order/fetch/all', 2)({ props: { credentials: meesho.violina } })
//     result.pendingOrder$.subscribe({
//       next: orders => {
//         console.log(orders.data.orders)
//       },
//       complete: done
//     })
//   })
// })

// describe('Fetch Returns', () => {
//   it('All Returns', async (done) => {
//     const result = fetchMeeshoReturns('return$')({ props: { credentials: meesho.anchor } })
//     result.return$.subscribe({
//       next: console.log,
//       complete: done
//     })
//     // done()
//   })
// })

// describe('Fetch Penalties', () => {
//   it('Meesho Penalties', async (done) => {
//     const result = fetchMeeshoPenalties('previousPenalties$')({ props: {credentials: meesho.anchor}})
//     result.previousPenalties$.subscribe({
//       next: console.log,
//       complete: done
//     })    
//   })
// })

// describe('Fetch Previous Invoices', () => {
//   it('Meesho Invoices', async (done) => {
//     const result = fetchMeeshoInvoices('previousInvoices$')({props: {credentials: meesho.anchor}})
//     result.previousInvoices$.subscribe({
//       next: console.log,
//       complete: done
//     })
//   })
// })

// describe('Fetch Outstanding and Previous Payments', () => {
//   it('Meesho Outstanding and Previous Payments', async (done) => {
//     const result = fetchMeeshoOutstandingPreviousPayments('outstandingPreviousPayments$', 'pending', 'due-payments')({ props: {credentials: meesho.anchor}})
//     result.outstandingPreviousPayments$.subscribe({
//       next: console.log,
//       complete: done
//     })
//   })
// })

// describe('Fetch Payment Details', () => {
//   it('Meesho Next and Last Payment Details', async (done) => {
//     const result = fetchMeeshoPaymentDetails('nextLastPaymentDetails$', '/1/','2019-12-18', 'paid')({props: {credentials: meesho.anchor}})
//     result.nextLastPaymentDetails$.subscribe({
//       next: console.log,
//       complete: done
//     })
//   })
// })

// describe('Fetch shipped orders invoice file link', () => {
//   it('Meesho shipped orders invoice file link download', async (done) => {
//     const result = fetchShippedOrdersFileUrl('invoiceUrl$', '2019-05-01', '2020-02-04')({props: { credentials: meesho.anchor}})
//     result.invoiceUrl$.subscribe({
//       next:console.log,
//       complete: done,
//     })
//   })
// })

// })

describe('Amazon', () => {
  describe('Pricing Module', () => {
    it('fetch my fees', (done) => {
      FT.run([
        createAmazonAuthfetch,
        fetchAmazonMyFeeEstimate$,
        function subscribe({ props }) {
          return props.feeEstimate$.toPromise().then(val => {
            console.log(JSON.stringify(val))
          })
        }
      ], {
        credentials: amazonin.anchor,
        asin: 'B075FSBWQF',
        amount: 1000,
        type: 'MARKETPLACE'
      }).then(() => done()).catch(done)
    }, 200000)

    it('fetch competitive pricing', (done) => {
      FT.run([
        createAmazonAuthfetch,
        fetchAmazonCompetitivePricing$,
        function subscribe({ props }) {
          return props.competitivePricing$.toPromise().then(val => {
            console.log(JSON.stringify(val))
          })
        }
      ], {
        credentials: amazonin.anchor,
        asin: 'B075FSBWQF'
      }).then(() => done()).catch(done)
    }, 200000)

    it('fetch product categories', (done) => {
      FT.run([
        createAmazonAuthfetch,
        fetchAmazonProductCategories$,
        function subscribe({ props }) {
          return props.productCategories$.toPromise().then(val => {
            console.log(JSON.stringify(val))
          })
        }
      ], {
        credentials: amazonin.anchor,
        asin: 'B075FSBWQF'
      }).then(() => done()).catch(done)
    }, 200000)

    it('fetch matching product', (done) => {
      FT.run([
        createAmazonAuthfetch,
        fetchAmazonMatchingProduct$,
        function subscribe({ props }) {
          return props.matchingProduct$.toPromise().then(val => {
            console.log(JSON.stringify(val))
          })
        }
      ], {
        credentials: amazonin.anchor,
        asin: 'B075FSBWQF'
      }).then(() => done()).catch(done)
    }, 200000)

    it('fetch lowest Offer Listings', (done) => {
      FT.run([
        createAmazonAuthfetch,
        fetchAmazonLowestOfferListings$,
        function subscribe({ props }) {
          return props.lowestOfferListings$.toPromise().then(val => {
            console.log(JSON.stringify(val))
          })
        }
      ], {
        credentials: amazonin.anchor,
        asin: 'B07KJ61J2F'
      }).then(() => done()).catch(done)
    }, 200000)

    it('fetch lowest Priced Offerings', (done) => {
      FT.run([
        createAmazonAuthfetch,
        fetchAmazonGetLowestPricedOffers$,
        function subscribe({ props }) {
          return props.lowestPricedOffers$.toPromise().then(val => {
            console.log(JSON.stringify(val))
          })
        }
      ], {
        credentials: amazonin.anchor,
        asin: 'B07KJ61J2F'
      }).then(() => done()).catch(done)
    }, 200000)
  })
})
