import * as assert from 'assert'
import { sum } from '../src/basic'

describe('Checking Tests', () => {
  it('first sum test', () => {
    assert.deepStrictEqual(sum(2, 2), 4)
  })
})
