---
title: Core Concepts
parent: Introduction
nav_order: 1
---

# Core Concepts

Empower Go Online developers to write pure FP library built atop higher order abstractions.

---

Don't panic. If "higher order abstractions" sound like insanely complicated words to you, the first thing you should know is that you don't need to understand all the intricate details, you can refer to recipes to integrate patterns. There is a learning curve, certainly, but don't fear that you can't learn to program effectively, even as a beginner in functional programming. The trick is to start using recipes that are easier to understand and then gradually expand your knowledge.

There's a list of [recipes](../recipes/) to help you find quick solutions to common problems.

