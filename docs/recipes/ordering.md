---
title: Ordering
parent: Recipes
nav_order: 2
---

# How to determine the order of data

If you need to decide on the order of two values, you can make use of the `compare` method provided by `Ord` instances. Ordering builds on [equality](./equality).

Note that `compare` returns an Ordering, which is one of these values `-1 | 0 | 1`. We say that

* `x < y` if and only if `compare(x, y)` is equal to `-1`
* `x` is equal to `y` if and only if `compare(x, y)` is equal to `0`
* `x > y` if and only if `compare(x, y)` is equal to `1`

## Primitive comparisons

```ts
import { ordBoolean, ordDate, ordNumber, ordString } from 'fp-ts/lib/Ord'

ordNumber.compare(4, 5) // -1
ordNumber.compare(5, 5) // 0
ordNumber.compare(6, 5) // 1

ordBoolean.compare(true, false) // 1
ordDate.compare(new Date('1990-01-22'), new Date('1989-08-10')) // 1
ordString.compare('Rishab', 'Lavanya') // -1
```

Note that all `Ord` instances also define the `equals` method, because it is a prerequisite to be able to compare data.

```ts
ordBoolean.equals(false, false) // true
```

## Custom comparisons

You can create custom comparisons using `fromCompare` like so:

```ts
import { fromCompare } from 'fp-ts/lib/Ord'

const strlenOrd = fromCompare((a: string, b: string) => (a.length < b.length ? -1 : a.length > b.length ? 1 : 0))
strlenOrd.compare('Hi', 'there') // -1
strlenOrd.compare('Goodbye', 'friend') // 1
```

But most of the time, you can achieve the same result in a simpler way with `contramap`:

```ts
import { contramap, ordNumber } from 'fp-ts/lib/Ord'

const strlenOrd = contramap((s: string) => s.length)(ordNumber)
strlenOrd.compare('Hi', 'there') // -1
strlenOrd.compare('Goodbye', 'friend') // 1
```
