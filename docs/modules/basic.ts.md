---
title: basic.ts
nav_order: 1
parent: Modules
---

# basic overview

Testing Docs build

Added in v1.0.0

---

<h2 class="text-delta">Table of contents</h2>

- [sum (function)](#sum-function)

---

# sum (function)

Testing Docs build

**Signature**

```ts
export function sum(a: number, b: number): number { ... }
```

Added in v1.0.0
