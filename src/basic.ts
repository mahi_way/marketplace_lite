/**
 * Testing Docs build
 *
 * @since 1.0.0
 */

export function sum(a: number, b: number): number {
  return a + b
}
