import { merge } from 'rxjs'
import { concatMap } from 'rxjs/operators'
import retry from 'async-retry'
import axios from 'axios'
import {
  parseHeader,
  fetchOrderByOrderItemIds$,
  fetchPreShipments$,
  fetchPostShipments$,
  fetchReturns$,
  fetchCancelledShipments$
} from './base'

export const createHeader = ({ props: { credentials } }: any) => ({
  headers: parseHeader(credentials)
})

export const checkFlipkartLogin = async ({ props: { headers, locationList }, path }) => {
  if (locationList) {
    return path.valid()
  }

  const header = {
    Accept: headers.Accept,
    'Accept-Language': headers['Accept-Language'],
    Connection: headers.Connection,
    Cookie: headers.Cookie,
    Host: 'seller.flipkart.com',
    Referer: 'https://seller.flipkart.com/index.html',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
    'User-Agent':
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
    'X-Requested-With': 'XMLHttpRequest'
  }

  let result
  try {
    await retry(
      async () => {
        result = (
          await axios.request({
            url: `https://seller.flipkart.com/napi/get-locations?locationType=pickup&include=state&capabilities=NON_FBF%2CFBF_LITE`,
            headers: header
          })
        ).data
      },
      {
        randomize: true,
        onRetry: err => {
          // tslint:disable-next-line: no-console
          console.log('RETRYING FLIPKART GETLOCATION')
          // tslint:disable-next-line: no-console
          console.log(err)
        }
      }
    )
  } catch (err) {
    // return path.invalid({ error: err });
    throw new Error(err)
  }

  if (typeof result === 'string') {
    return path.invalid()
  }

  return path.valid({
    locationList: (result && result.result && result.result.multiLocationList) || []
  })
}

export const fetchSingleOrderItem = async ({ props: { headers, orderId } }: any) => ({
  orderItems$: fetchOrderByOrderItemIds$(headers)(orderId)
})

export const fetchOrderItem = async ({ props: { headers, mappedShipments$ } }: any) => ({
  orderItems$: mappedShipments$.pipe(
    concatMap(
      ({ orderItemId }) => fetchOrderByOrderItemIds$(headers)(orderItemId),
      (order: any, orderItems) => ({ ...order, item: orderItems[0] })
    )
  )
})

export const fetchShipments = async ({ props: { headers, params } }: any) => ({
  shipments$: merge(
    fetchPreShipments$(headers)(params),
    fetchPostShipments$(headers)(params),
    fetchCancelledShipments$(headers)(params)
  )
})

export const fetchReturns = async ({
  props: {
    headers,
    params: { date }
  }
}: any) => ({
  returns$: merge(
    fetchReturns$(headers)({ type: 'customer_return', date }),
    fetchReturns$(headers)({ type: 'courier_return', date })
  )
})

export const mapShipments = async ({ props: { shipments$ } }: any) => ({
  mappedShipments$: shipments$.pipe(
    concatMap(({ orderItems, subShipments, ...rest }) =>
      orderItems.map((order, index) => ({
        ...rest,
        orderItemId: order.orderItemId,
        order,
        subShipments: subShipments[index]
      }))
    )
  )
})

// export const subscribeOrderItems = async ({ props: { orderItems$ } }: any) =>
//   new Promise(resolve => {
//     orderItems$
//       .pipe(
//         toArray()
//         // catchError(err => {
//         //   console.log('*****');
//         //   console.log(err);
//         //   return empty();
//         // })
//       )
//       .subscribe(orderItems => {
//         resolve({ orderItems })
//       })
//   })

// export const subscribeReturns = async ({ props: { returns$ } }: any) =>
//   new Promise(resolve => {
//     returns$.pipe(toArray()).subscribe(returns => {
//       resolve({ returns })
//     })
//   })
