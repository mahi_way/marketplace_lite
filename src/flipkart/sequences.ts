import { sequence } from 'function-tree'

import {
  createHeader,
  fetchSingleOrderItem,
  fetchOrderItem,
  fetchShipments,
  mapShipments,
  // subscribeOrderItems,
  fetchReturns
  // subscribeReturns
} from './actions'

// export const Sequences = 'FLIPKART_SELLER_API_FUNCTION_TREE_SEQUENCES'

export const fetchFlipkartSingleOrder = sequence('Fetch Flipkart Single Order By OrderItemId', [
  createHeader,
  fetchSingleOrderItem
])

export const fetchFlipkartOrders = sequence('Fetch Flipkart Orders', [
  createHeader,
  fetchShipments,
  mapShipments,
  fetchOrderItem
  // subscribeOrderItems
])

export const fetchFlipkartReturns = sequence('Fetch Flipkart Returns', [
  createHeader,
  fetchReturns
  // subscribeReturns
])
