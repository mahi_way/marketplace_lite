import { empty } from 'rxjs'
import { filter, map, expand, concatMap } from 'rxjs/operators'
import { compose ,pick } from 'ramda'
import { RxHR } from '../../utils/rx-http-request'
/*
 * ---
 * Constants
 * ---
 */
const apiurl = 'https://api.flipkart.net/sellers'

/*
 * ----
 * Helpers
 * ----
 */

const constructHeader = ({ accesskey, secret, bearer }) => ({
  'Content-Type': 'application/vnd.slc+json',
  Accept: 'application/json',
  appid: accesskey,
  'app-secret': secret,
  Authorization: `bearer ${bearer}`
})

const getRequest$ = (headers, url) => RxHR.get(url, {
  headers,
  json: true
}).pipe(
  // tap(console.log),
  map(data => data.response),
  filter((response: any) => response.statusCode === 200),
  map((response: any) => response.body)
)

const postRequest$ = (headers, url, body) => RxHR.post(url, {
  headers,
  json: true,
  body
}).pipe(
  map(data => data.response),
  filter((response: any) => response.statusCode === 200),
  map((response: any) => response.body)
)

// export const fetchOrdersByOrderItemIds = headers => id => fetchURL({
//   url: `${apiurl}/v2/orders?orderItemIds=${id}`,
//   headers
// });

/*
 * ----
 * Exports
 * ----
 */

export const fetchOrderByOrderItemIds$ = headers => id => getRequest$(headers, `${apiurl}/v2/orders?orderItemIds=${id}`).pipe(
  map((response: any) => response.orderItems ? Array.isArray(response.orderItems) ? response.orderItems : [response.orderItems] : [])
)

export const fetchPreShipments$ = headers => ({ from, to }) => postRequest$(headers, `${apiurl}/v3/shipments/filter`, {
  filter: {
    type: 'preDispatch',
    // serviceProfiles: 'FBF_LITE',
    states: ['APPROVED', 'PACKING_IN_PROGRESS', 'PACKED', 'FORM_FAILED', 'READY_TO_DISPATCH'],
    orderDate: {
      from,
      to
    }
  }
}).pipe(
  expand(({ hasMore, nextPageUrl }) => hasMore ? getRequest$(headers, `${apiurl}${nextPageUrl}`) : empty()),
  concatMap(({ shipments }) => shipments)
)

export const fetchPostShipments$ = headers => ({ from, to }) => postRequest$(headers, `${apiurl}/v3/shipments/filter`, {
  filter: {
    type: 'postDispatch',
    // serviceProfiles: 'FBF_LITE',
    states: ['SHIPPED', 'DELIVERED'],
    orderDate: {
      from,
      to
    }
  }
}).pipe(
  expand(({ hasMore, nextPageUrl }) => hasMore ? getRequest$(headers, `${apiurl}${nextPageUrl}`) : empty()),
  concatMap(({ shipments }) => shipments)
)

export const fetchCancelledShipments$ = headers => ({ from, to }) => postRequest$(headers, `${apiurl}/v3/shipments/filter`, {
  filter: {
    type: 'cancelled',
    states: ['CANCELLED'],
    // serviceProfiles: 'FBF_LITE',
    cancellationType: 'buyerCancellation',
    orderDate: {
      from,
      to
    }
  }
}).pipe(
  expand(({ hasMore, nextPageUrl }) => hasMore ? getRequest$(headers, `${apiurl}${nextPageUrl}`) : empty()),
  concatMap(({ shipments }) => shipments)
)

export const fetchReturns$ = headers => ({ type, date }) => getRequest$(headers, `${apiurl}/v2/returns?source=${type}&createdAfter=${date}`).pipe(
  expand(({ hasMore, nextPageUrl }) => hasMore ? getRequest$(headers, `${apiurl}${nextPageUrl}`) : empty()),
  concatMap(({ returnItems }) => returnItems)
)

export const parseHeader = credentials =>
  compose(
    constructHeader,
    pick(['accesskey', 'secret', 'bearer'])
  )(credentials)
