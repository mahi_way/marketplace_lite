import * as puppeteer from 'puppeteer'
import { fromEvent, zip, race, throwError, timer, empty } from 'rxjs'
import { filter, map, take, flatMap, expand } from 'rxjs/operators'
import {
  fetchOrders,
  OrderFilters,
  fetchReturns,
  ReturnFilters,
  SortOrder,
  fetchPenalties,
  fetchInvoices,
  fetchOutstandingPreviousPayments,
  fetchPayments,
  fetchOrderShippedUrl
} from './helpers'
// import { meesho } from '../../credentials'
/**
 * Fetch Meesho Login Credentials By Logging into meesho using puppeteer
 *
 * @since 1.0.0
 */
export async function fetchMeeshoLoginCredentials({
  props: { credentials: { username = '', password = '' } }
}: {
  props: { credentials: { username: string; password: string } }
}): Promise<{ result: { cookies: string; sellerId: number; marketplaceId: string } }> {
  const browser = await puppeteer.launch({ headless: true })
  const page = await browser.newPage()

  return page
    .goto('https://supplier.meeshosupply.com/login')
    .then(() => page.waitForSelector('.login-input'))
    .then(() => page.$('.login-input'))
    .then(userInput => userInput.type(username))
    .then(() => page.$('.login-password'))
    .then(passwordInput => passwordInput.type(password))
    .then(() => page.click('.login-button'))
    .then(() => {
      const request$ = fromEvent(page, 'request').pipe(
        map((request: { postData: Function }) => request.postData()),
        filter(val => !!val),
        map((val: string) => {
          try {
            return JSON.parse(val)
          } catch (err) {
            return null
          }
        }),
        filter(val => !!val)
      )

      return race(
        timer(10000).pipe(flatMap(() => throwError('Could not fetch sellerId and marketplaceId'))),
        zip(
          request$.pipe(
            filter((val: { identifier: string }) => !!val.identifier),
            map(({ identifier }: { identifier: string }) => identifier),
            take(1)
          ),
          request$.pipe(
            filter((val: { supplier_id: number }) => !!val.supplier_id),
            map(({ supplier_id }: { supplier_id: number }) => supplier_id),
            take(1)
          )
        )
      ).toPromise()
    })
    .then(async (val: [string, number]) => {
      await page.goto(`https://supplier.meeshosupply.com/${val[1]}/orders`)
      const cookie = await page.cookies()
      await browser.close()

      return {
        result: {
          marketplaceId: val[0],
          sellerId: val[1],
          cookies: cookie.map(c => `${c.name}=${c.value}`).join('; ')
        }
      }
    })
}

export function fetchMeeshoOrders(selector: string, url: string, status: number) {
  return function fetchMeeshoOrders$({
    props: { credentials, filter }
  }: {
    props: { credentials: { marketplaceId: string; sellerId: number; cookies: string }; filter?: OrderFilters }
  }): any {
    return {
      [selector]: fetchOrders({
        url,
        cookie: credentials.cookies,
        identifier: credentials.marketplaceId,
        supplierId: credentials.sellerId,
        status,
        filter
      })
    }
  }
}

export function fetchMeeshoReturns(selector: string, size?: number, sortOrder?: SortOrder) {
  return function fetchMeeshoReturns$({
    props: { credentials, filter }
  }: {
    props: { credentials: { marketplaceId: string; sellerId: number; cookies: string }; filter?: ReturnFilters }
  }): any {
    return {
      [selector]: fetchReturns({
        cookie: credentials.cookies,
        identifier: credentials.marketplaceId,
        supplierId: credentials.sellerId,
        filter,
        size,
        sortOrder
      }).pipe(
        expand(response =>
          response.paging.next
            ? fetchReturns({
                cookie: credentials.cookies,
                identifier: credentials.marketplaceId,
                supplierId: credentials.sellerId,
                pagingToken: response.paging.next
              })
            : empty()
        )
      )
    }
  }
}

export function fetchMeeshoPenalties(selector: string, size?: string, offset?: string) {
  return function fetchMeeshoPenalties$({
    props: { credentials }
  }: {
    props: { credentials: { marketplaceId: string; cookies: string } }
  }): any {
    return {
      [selector]: fetchPenalties({
        cookie: credentials.cookies,
        identifier: credentials.marketplaceId,
        size,
        offset
      })
    }
  }
}

export function fetchMeeshoInvoices(selector: string) {
  return function fetchMeeshoInvoices$({
    props: { credentials }
  }: {
    props: { credentials: { marketplaceId: string; cookies: string } }
  }): any {
    return {
      [selector]: fetchInvoices({
        cookie: credentials.cookies,
        identifier: credentials.marketplaceId
      })
    }
  }
}

export function fetchMeeshoOutstandingPreviousPayments(selector: string, status: string, action: string) {
  return function fetchMeeshoOutstandingPreviousPayments$({
    props: { credentials }
  }: {
    props: { credentials: { marketplaceId: string; cookies: string } }
  }): any {
    return {
      [selector]: fetchOutstandingPreviousPayments({
        cookie: credentials.cookies,
        identifier: credentials.marketplaceId,
        status,
        action
      })
    }
  }
}

export function fetchMeeshoPaymentDetails(selector: string, typeCall: string, date: string, status: string) {
  return function fetchMeeshoPaymentDetails$({
    props: { credentials }
  }: {
    props: { credentials: { marketplaceId: string; sellerId: string; cookies: string } }
  }): any {
    return {
      [selector]: fetchPayments({
        cookie: credentials.cookies,
        identifier: credentials.marketplaceId,
        supplierId: credentials.sellerId,
        typeCall,
        date,
        status
      })
    }
  }
}

export function fetchShippedOrdersFileUrl(selector: string, min: string, max: string) {
  return function fetchShippedOrdersFileUrl$({
    props: { credentials }
  }: {
    props: { credentials: { cookies: string; marketplaceId: string } }
  }): any {
    return {
      [selector]: fetchOrderShippedUrl({
        cookie: credentials.cookies,
        identifier: credentials.marketplaceId,
        min,
        max
      })
    }
  }
}

// const { pendingOrder$ } = fetchMeeshoOrders('pendingOrder$', 'https://supplier.meeshosupply.com/api/v2/order/fetch/all', 6)({ props: { credentials: meesho.violina } })
// pendingOrder$.subscribe(val => {
//   console.log(JSON.stringify(val))
// })
