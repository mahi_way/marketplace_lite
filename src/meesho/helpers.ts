import { from, Observable } from 'rxjs'
import { filter as rxFilter, map } from 'rxjs/operators'
import axiosAPI, { AxiosRequestConfig } from 'axios'

export enum SlaStatus {
  breached = 'breached',
  sla_others = 'sla_others',
  breaching_soon = 'breaching_soon'
}

export enum SortOrder {
  ASC = 'ASC',
  DESC = 'DESC'
}

export interface OrderFilters {
  order_date: {
    min: string,
    max: string
  },
  sla_status: [SlaStatus]
}

export interface ReturnFilters {
  pickup_date: {
    min: string,
    max: string
  },
  delivered_date: {
    min: string,
    max: string
  }
}

interface FetchOrdersConfig {
  url: string,
  status?: number,
  cookie: string,
  identifier: string,
  supplierId: number,
  filter?: OrderFilters
}

interface FetchReturnsConfig {
  cookie: string,
  identifier: string,
  supplierId: number,
  filter?: ReturnFilters,
  pagingToken?: string,
  size?: number,
  sortOrder?: SortOrder
}

interface FetchPenaltiesConfig {
  cookie: string,
  identifier: string,
  size?: string,
  offset?: string
}

export interface ReturnResponse {
  paging: {
    next: string
  },
  returned_suborder_data_list: Array<object>
}

interface FetchOutPrevPaymentsConfig{
  action: string, 
  status: string, 
  identifier: string, 
  cookie: string
}

interface FetchPaymentsConfig{
  cookie: string,
  identifier: string,
  supplierId: string,
  typeCall: string,
  date: string,
  status: string,
}

// -------------------------------------------------------------------
/**
 * Fetch Meesho Orders
 *
 * @param status
 *
 * @returns Observable
 */
export function fetchOrders({ url, status = 0, cookie, identifier, supplierId, filter = null }: FetchOrdersConfig): Observable<any> {

  const axiosConfig: AxiosRequestConfig = {
    url,
    method: 'post',
    headers: {
      'authority': 'supplier.meeshosupply.com',
      'method': 'post',
      'cache-control': 'no-cache',
      'pragma': 'no-cache',
      'scheme': 'https',
      'accept': 'application/json',
      'accept-encoding': 'gzip, deflate, br',
      'accept-language': 'en-US,en;q=0.9',
      'content-type': 'application/json;charset=UTF-8',
      'cookie': cookie,
      'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
      'origin': 'https://supplier.meeshosupply.com',
      'referer': 'https://supplier.meeshosupply.com/ovifl/orders',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
    },
    data: filter ? {
      identifier,
      supplier_id: supplierId,
      status,
      filter
    } : {
        identifier,
        supplier_id: supplierId,
        status
      }
  }
  return from(axiosAPI.request(axiosConfig)).pipe(
    rxFilter(response => response.status === 200),
    map(response => response.data)
  )
}

export interface ReturnsData {
  identifier: string,
  supplier_id: number,
  filter?: ReturnFilters,
  size: number,
  sort_order: string,
  __paging_token?: string
}

/**
 * Fetch Meesho Returns
 *
 * @param status
 * @returns Observable
 */
export function fetchReturns({ pagingToken = null, cookie, identifier, supplierId, filter = null, size = 100, sortOrder = SortOrder.DESC }: FetchReturnsConfig): Observable<any> {

  let data: ReturnsData = {
    identifier,
    supplier_id: supplierId,
    size,
    sort_order: sortOrder
  }

  if (filter) {
    data.filter = filter
  }

  if (pagingToken) {
    data.__paging_token = pagingToken
  }

  const axiosConfig: AxiosRequestConfig = {
    url: 'https://supplier.meeshosupply.com/api/fetch/returned/subOrders',
    method: 'post',
    headers: {
      'authority': 'supplier.meeshosupply.com',
      'method': 'post',
      'cache-control': 'no-cache',
      'pragma': 'no-cache',
      'scheme': 'https',
      'accept': 'application/json',
      'accept-encoding': 'gzip, deflate, br',
      'accept-language': 'en-US,en;q=0.9',
      'content-type': 'application/json;charset=UTF-8',
      'cookie': cookie,
      'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
      'origin': 'https://supplier.meeshosupply.com',
      'referer': 'https://supplier.meeshosupply.com/ovifl/orders/returned',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
    },
    data
  }
  return from(axiosAPI.request(axiosConfig)).pipe(
    rxFilter(response => response.status === 200),
    map(response => response.data)
  )
}

export function fetchPenalties({ cookie, identifier, size = "10", offset = "0" }: FetchPenaltiesConfig): Observable<any> {
  const axiosConfig: AxiosRequestConfig = {
    url: 'https://supplier.meeshosupply.com/api/payments-new/penalties',
    method: 'post',
    headers: {
      'authority': 'supplier.meeshosupply.com',
      'method': 'post',
      'cache-control': 'no-cache',
      'pragma': 'no-cache',
      'scheme': 'https',
      'accept': 'application/json',
      'accept-encoding': 'gzip, deflate, br',
      'accept-language': 'en-US,en;q=0.9',
      'content-type': 'application/json;charset=UTF-8',
      'cookie': cookie,
      'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
      'origin': 'https://supplier.meeshosupply.com',
      'referer': 'https://supplier.meeshosupply.com/ovifl/payment/penalties',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
    },
    data: { "supplierIdentifier": identifier, "limit": size, "offset": offset }
  }
  return from(axiosAPI.request(axiosConfig)).pipe(
    rxFilter(response => response.status === 200),
    map(response => response.data));
}

export function fetchInvoices({ cookie, identifier }: { cookie: string, identifier: string }): Observable<any> {
  const axiosConfig: AxiosRequestConfig = {
    url: 'https://supplier.meeshosupply.com/api/payments/invoices',
    method: 'post',
    headers: {
      'authority': 'supplier.meeshosupply.com',
      'method': 'post',
      'cache-control': 'no-cache',
      'pragma': 'no-cache',
      'scheme': 'https',
      'accept': 'application/json',
      'accept-encoding': 'gzip, deflate, br',
      'accept-language': 'en-US,en;q=0.9',
      'content-type': 'application/json;charset=UTF-8',
      'cookie': cookie,
      'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
      'origin': 'https://supplier.meeshosupply.com',
      'referer': 'https://supplier.meeshosupply.com/ovifl/payment/previous-invoices',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
    },
    data: { 'identifier': identifier }
  }
  return from(axiosAPI.request(axiosConfig)).pipe(
    rxFilter(response => response.status === 200),
    map(response => response.data)
  )
}

export function fetchOutstandingPreviousPayments({action, status, identifier, cookie} : FetchOutPrevPaymentsConfig): Observable<any> {
  const axiosConfig: AxiosRequestConfig = {
    url: 'https://supplier.meeshosupply.com/api/payments-new/all',
    method: 'post',
    headers: {
      'authority': 'supplier.meeshosupply.com',
      'method': 'post',
      'cache-control': 'no-cache',
      'pragma': 'no-cache',
      'scheme': 'https',
      'accept': 'application/json',
      'accept-encoding': 'gzip, deflate, br',
      'accept-language': 'en-US,en;q=0.9',
      'content-type': 'application/json;charset=UTF-8',
      'cookie': cookie,
      'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
      'origin': 'https://supplier.meeshosupply.com',
      'referer': 'https://supplier.meeshosupply.com/ovifl/payment/' + action,
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
    },
    data: { "identifier": identifier, "status": status }
  }
  return from(axiosAPI.request(axiosConfig)).pipe(
    rxFilter(response => response.status === 200),
    map(response => response.data)
  )
}

export function fetchPayments({cookie, identifier, supplierId, typeCall, date, status}: FetchPaymentsConfig): Observable<any>{
  const axiosConfig: AxiosRequestConfig = {
    url: 'https://supplier.meeshosupply.com/api/payment-new/payment-details',
    method: 'post',
    headers: {
        'authority': 'supplier.meeshosupply.com',
        'method': 'post',
        'cache-control': 'no-cache',
        'pragma': 'no-cache',
        'scheme': 'https',
        'accept': 'application/json',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.9',   
        'content-type': 'application/json;charset=UTF-8',
        'cookie': cookie,
        'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
        'origin': 'https://supplier.meeshosupply.com',   
        'referer': 'https://supplier.meeshosupply.com/api/payment-new/payment-details' + typeCall + date,
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
    },
    data: { "identifier": identifier, "supplier_id": supplierId, "status": status, "limit": 1000, "date": date }
  }
  return from(axiosAPI.request(axiosConfig)).pipe(
    rxFilter(response => response.status === 200),
    map(response => response.data)
  )
}

export function fetchOrderShippedUrl({cookie, identifier, min, max}: {cookie: string, identifier: string, min: string, max: string}): Observable<any>{
  const axiosConfig: AxiosRequestConfig = {
    url: 'https://supplier.meeshosupply.com/api/order/gst/details',
        method: 'post',
        headers: {
            'authority': 'supplier.meeshosupply.com',
            'method': 'POST',
            'path': '/api/order/gst/details',
            'scheme': 'https',
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',           
            'content-type': 'application/json;charset=UTF-8',
            'cookie': cookie,
            'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
            'origin': 'https://supplier.meeshosupply.com',
            'pragma': 'no-cache',
            'referer': 'https://supplier.meeshosupply.com/ovifl/orders',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
        },
        data: { "identifier": identifier, 
            "order_date":{
                'min': min,
                'max': max,
            }
          }
    }
  return from(axiosAPI.request(axiosConfig)).pipe(
    rxFilter(response => response.status === 200),
    map(response => response.data.data.invoice_url)
  )
}
