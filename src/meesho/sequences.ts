import { sequence } from 'function-tree'
import { fetchMeeshoOrders } from './actions'

export const fetchMeeshoOrdersSequence = sequence('Fetch Meesho Orders', [
  fetchMeeshoOrders('pendingOrder$', 'https://supplier.meeshosupply.com/api/v1/order/fetch/all', 0),
  fetchMeeshoOrders('dispacthableOrder$', 'https://supplier.meeshosupply.com/api/v1/order/fetch/all', 0),
  fetchMeeshoOrders('readyToShipOrder$', 'https://supplier.meeshosupply.com/api/v1/order/fetch/all', 0),
  function mergeObs({ props }) {
    console.log(props)
  }
])
