export default {
  'US': {
    'name': 'Amazon United States',
    'site': 'amazon.com',
    'id': 'ATVPDKIKX0DER',
    'mws_endpoint': 'mws.amazonservices.com',
    'region': 'NA'
  },
  'CA': {
    'name': 'Amazon Canada',
    'site': 'amazon.ca',
    'id': 'A2EUQ1WTGCTBG2',
    'mws_endpoint': 'mws.amazonservices.com',
    'region': 'NA'
  },
  'MX': {
    'name': 'Amazon Mexico',
    'site': 'amazon.com.mx',
    'id': 'A1AM78C64UM0Y8',
    'mws_endpoint': 'mws.amazonservices.com',
    'region': 'NA'
  },
  'DE': {
    'name': 'Amazon Germany',
    'site': 'amazon.de',
    'id': 'A1PA6795UKMFR9',
    'mws_endpoint': 'mws-eu.amazonservices.com',
    'region': 'EU'
  },
  'ES': {
    'name': 'Amazon Spain',
    'site': 'amazon.es',
    'id': 'A1RKKUPIHCS9HS',
    'mws_endpoint': 'mws-eu.amazonservices.com',
    'region': 'EU'
  },
  'FR': {
    'name': 'Amazon France',
    'site': 'amazon.fr',
    'id': 'A13V1IB3VIYZZH',
    'mws_endpoint': 'mws-eu.amazonservices.com',
    'region': 'EU'
  },
  'IT': {
    'name': 'Amazon Italy',
    'site': 'amazon.it',
    'id': 'APJ6JRA9NG5V4',
    'mws_endpoint': 'mws-eu.amazonservices.com',
    'region': 'EU'
  },
  'UK': {
    'name': 'Amazon UK',
    'site': 'amazon.co.uk',
    'id': 'A1F83G8C2ARO7P',
    'mws_endpoint': 'mws-eu.amazonservices.com',
    'region': 'EU'
  },
  'IN': {
    'name': 'Amazon India',
    'site': 'amazon.in',
    'id': 'A21TJRUUN4KGV',
    'mws_endpoint': 'mws.amazonservices.in',
    'region': 'IN'
  },
  'AU': {
    'name': 'Amazon Australia',
    'site': 'amazon.com.au',
    'id': 'A39IBJ37TRP1C6',
    'mws_endpoint': 'mws.amazonservices.com.au',
    'region': 'FE'
  },
  'JP': {
    'name': 'Amazon Japan',
    'site': 'amazon.co.jp',
    'id': 'A1VC38T7YXB528',
    'mws_endpoint': 'mws.amazonservices.jp',
    'region': 'FE'
  },
  'CN': {
    'name': 'Amazon China',
    'site': 'amazon.cn',
    'id': 'AAHKV2X7AFYLW',
    'mws_endpoint': 'mws.amazonservices.com.cn',
    'region': 'CN'
  },
  'BR': {
    'name': 'Amazon Brazil',
    'site': 'amazon.com.br',
    'id': 'A2Q3Y263D00KWC',
    'mws_endpoint': 'mws.amazonservices.com',
    'region': 'BR'
  }
}
