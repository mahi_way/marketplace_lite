import { MWSClientBase } from './base'

const crypto = require('crypto')
const request = require('request')
// const buffer = require('buffer')
const iconv = require('iconv-lite')
const ACTIONS = require('./actions')
const SEQUENCES = require('./sequences')

export class NodeJSMWSClient extends MWSClientBase {
  calcMD5(content): string {
    let hash = crypto.createHash('md5')
    hash.update(content)
    return hash.digest('base64')
  }

  calcHMAC(content, secret): string {
    let hmac = crypto.createHmac('sha256', secret)
    hmac.update(content)
    return hmac.digest('base64')
  }

  makeHttpRequest(method, url, headers, body, cbk): void {
    let options = {
      method: method,
      url: url,
      headers: headers,
      body: body
    }
    return request(options, function(err, res) {
      // let parsed_err = null
      // let parsed_res = null
      if (err) {
        cbk(err, null)
      } else {
        cbk(MWSClientBase.parseResponseError(res.statusCode, res.headers, res.body), MWSClientBase.parseResponse(res.statusCode, res.headers, res.body))
      }
    })
  }

  makeHttpRequestFormData(url, headers, form, cbk): void {
    let options = {
      url: url,
      headers: headers,
      form: form
    }
    return request.post(options, function(err, res) {
      // let parsed_err = null
      // let parsed_res = null
      if (err) {
        cbk(err, null)
      } else {
        cbk(MWSClientBase.parseResponseError(res.statusCode, res.headers, res.body), MWSClientBase.parseResponse(res.statusCode, res.headers, res.body))
      }
    })
  }

  encodeContent(content: string, encoding: string) {
    return iconv.encode(content, encoding)
  }

  getUserAgent() {
    return 'EaseClient/' + MWSClientBase.version() + ' (Language=Javascript; Platform=NodeJS ' + process.version + ')'
  }
}

// exports.NodeJSMWSClient=NodeJSMWSClient;
// exports.ACTIONS = actions;
// exports.SEQUENCES = sequences;
module.exports = {
  NodeJSMWSClient,
  ACTIONS,
  SEQUENCES
}
