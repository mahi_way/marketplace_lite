import { compose, tail, split } from 'ramda'
import { from, empty, forkJoin } from 'rxjs'
import { expand, delay, concatMap, map, skip, bufferCount } from 'rxjs/operators'
import * as parser from 'fast-xml-parser'
import { NodeJSMWSClient as MWSClient } from './nodejs'

const retry = require('retry')

// Helpers
const retryStrategyShort = {
  retries: 6,
  factor: 3,
  minTimeout: 2 * 1000
}

const retryStrategyMedium = {
  retries: 8,
  factor: 4,
  minTimeout: 2 * 1000
}

const retryStrategyLong = {
  retries: 10,
  factor: 5,
  minTimeout: 2 * 1000
}

/**
 * @param authfetch - MWSClient
 * @returns { valid, error }
 */
export const checkOrderServiceStatus = async ({ props: { authfetch } }) =>
  new Promise((resolve, reject) => {
    authfetch.ListMarketplaceParticipations(function(err, res) {
      if (err) {
        // console.log(err);
        const error = parser.parse(err.body)
        // console.log('Error -', error);
        reject(new Error(error.ErrorResponse.Error.Message))
      } else {
        if (res.status === 200) {
          resolve({ valid: true })
        } else {
          reject({ valid: false, error: res })
        }
      }
    })
  })

/**
 * @param credentials - { marketplace, accesskey, secret, sellerID, mwsAuthToken }
 */
export const createAmazonAuthfetch = async ({ props: { credentials } }) => ({
  authfetch: new MWSClient(
    compose(tail, split(' '))(credentials.marketplace),
    credentials.appId,
    credentials.appSecret,
    credentials.sellerId,
    credentials.authToken
  )
})

/*
 * ----
 * Observer Functions
 * ----
 */

const downloadReport$ = authfetch => reportId =>
  from(new Promise((resolve, reject) => {
    if (reportId === null) {
      resolve('')
    } else {
      let operation = retry.operation(retryStrategyLong)

      operation.attempt(function() {
        authfetch.GetReport({ ReportId: reportId }, (error, response) => {
          if (error) {
            if (operation.retry(error)) {
              return
            }
            reject(error)
          } else {
            // console.log('GOT DOWNLOAD RESPONSE');
            resolve(response.body)
          }
        })
      })
    }
  }))

const orderItems$ = authfetch => orderId =>
  from(new Promise((resolve, reject) => {
    let operation = retry.operation(retryStrategyShort)

    operation.attempt(function() {
      authfetch.ListOrderItems(orderId, (error, response) => {
        // console.log(orderId);
        if (error) {
          // console.log(error);
          if (operation.retry(error)) {
            return
          }
          reject(error)
        } else {
          // console.log('resolved');
          resolve((parser.parse(response.body)).ListOrderItemsResponse.ListOrderItemsResult)
        }
      })
    })
  }))

const orderListNext$ = authfetch => NextToken =>
  from(new Promise((resolve, reject) => {
    let operation = retry.operation(retryStrategyShort)

    operation.attempt(function() {
      authfetch.ListOrdersByNextToken({
        NextToken
      }, (error, response) => {
        if (error) {
          if (operation.retry(error)) {
            return
          }
          reject(error)
        } else {
          resolve((parser.parse(response.body)).ListOrdersByNextTokenResponse.ListOrdersByNextTokenResult)
        }
      })
    })
  }))

const competitivePricing$ = authfetch => args =>
  from(new Promise((resolve, reject) => {
    let operation = retry.operation(retryStrategyShort)

    operation.attempt(function() {
      authfetch.GetCompetitivePricingForASIN(args, (error, response) => {
        // console.log(orderId);
        if (error) {
          // console.log(error);
          if (error.status === 400) {
            reject(error)
          }
          if (operation.retry(error)) {
            return
          }
          reject(error)
        } else {
          // console.log('resolved');
          resolve((parser.parse(response.body)))
        }
      })
    })
  }))

const feeEstimate$ = authfetch => args =>
  from(new Promise((resolve, reject) => {
    let operation = retry.operation(retryStrategyShort)

    operation.attempt(function() {
      authfetch.GetMyFeesEstimate(args, (error, response) => {
        // console.log(orderId);
        if (error) {
          // console.log(error);
          if (error.status === 400) {
            reject(error)
          }
          if (operation.retry(error)) {
            return
          }
          reject(error)
        } else {
          // console.log('resolved');
          resolve((parser.parse(response.body)))
        }
      })
    })
  }))

const lowestOfferListings$ = authfetch => args =>
from(new Promise((resolve, reject) => {
  let operation = retry.operation(retryStrategyShort)

  operation.attempt(function() {
    authfetch.GetLowestOfferListingsForASIN(args, (error, response) => {
      if (error) {
        if (error.status === 400) {
          reject(error)
        }
        if (operation.retry(error)) {
          return
        }
        reject(error)
      } else {
        // console.log('resolved');
        resolve((parser.parse(response.body)))
      }
    })
  })
}))

// Handle error for 'Failed processing arguments of org.jboss.resteasy.spi.metadata.ResourceMethod'
// ref - https://stackoverflow.com/questions/36293752/getlowestpricedoffersforsku-failed-processing-arguments
const lowestPricedOffers$ = authfetch => args =>
from(new Promise((resolve, reject) => {
  let operation = retry.operation(retryStrategyShort)

  operation.attempt(function() {
    authfetch.GetLowestPricedOffersForASIN(args, (error, response) => {
      if (error) {
        if (error.status === 400) {
          reject(error)
        }
        if (operation.retry(error)) {
          return
        }
        reject(error)
      } else {
        // console.log('resolved');
        resolve((parser.parse(response.body)))
      }
    })
  })
}))

const matchingProduct$ = authfetch => args =>
from(new Promise((resolve, reject) => {
  let operation = retry.operation(retryStrategyShort)

  operation.attempt(function() {
    authfetch.GetMatchingProduct(args, (error, response) => {
      // console.log(orderId);
      if (error) {
        // console.log(error);
        if (error.status === 400) {
          reject(error)
        }
        if (operation.retry(error)) {
          return
        }
        reject(error)
      } else {
        // console.log('resolved');
        resolve((parser.parse(response.body)))
      }
    })
  })
}))

const productCategories$ = authfetch => args =>
  from(new Promise((resolve, reject) => {
    let operation = retry.operation(retryStrategyShort)

    operation.attempt(function() {
      authfetch.GetProductCategoriesForASIN(args, (error, response) => {
        // console.log(orderId);
        if (error) {
          // console.log(error);
          if (error.status === 400) {
            reject(error)
          }
          if (operation.retry(error)) {
            return
          }
          reject(error)
        } else {
          // console.log('resolved');
          resolve((parser.parse(response.body)))
        }
      })
    })
  }))

const reportListNext$ = authfetch => NextToken =>
  from(new Promise((resolve, reject) => {
    // console.log('REQUESTION REPORT');
    let operation = retry.operation(retryStrategyMedium)

    operation.attempt(function() {
      authfetch.GetReportListByNextToken({
        NextToken
      }, (error, response) => {
        if (error) {
          if (operation.retry(error)) {
            return
          }
          reject(error)
        } else {
          resolve((parser.parse(response.body)).GetReportListByNextTokenResponse.GetReportListByNextTokenResult)
        }
      })
    })
  }))

const reportResult$ = authfetch => reportId =>
  from(new Promise((resolve, reject) => {
    let operation = retry.operation(retryStrategyMedium)

    operation.attempt(function() {
      authfetch.GetReportRequestList({ 'ReportRequestIdList.Id.1': reportId }, (error, res) => {
        if (error) {
          if (operation.retry(error)) {
            return
          }
          reject(error)
        } else {
          const response = parser.parse(res.body, { parseTrueNumberOnly: true })
          // console.log(JSON.stringify(response))
          if (
            response.GetReportRequestListResponse.GetReportRequestListResult.ReportRequestInfo
              .ReportProcessingStatus === '_DONE_'
          ) {
            resolve(response.GetReportRequestListResponse.GetReportRequestListResult.ReportRequestInfo.GeneratedReportId)
          } else if (
            response.GetReportRequestListResponse.GetReportRequestListResult.ReportRequestInfo
                .ReportProcessingStatus === '_DONE_NO_DATA_'
          ) {
            resolve(null)
          } else if (
            response.GetReportRequestListResponse.GetReportRequestListResult.ReportRequestInfo
                .ReportProcessingStatus === '_CANCELLED_'
          ) {
            // console.log('AWESOME CANCELLED DATAAA');
            // console.log(response.GetReportRequestListResponse);
            // resolve(null);
            reject({
              status: 404,
              message: 'CANCELLED'
            })
          } else {
            if (operation.retry({
              status: 555,
              message: response
            })) {
              return
            }
            reject({
              status: 555,
              message: response
            })
          }
        }
      })
    })
  }))

/*
 * ----
 * Observer Actions
 * ----
 */

export const createAmazonOrderIdsBatch$ = async ({ props: { orderIds$ } }) => ({
  orderIdsBatch$: orderIds$.pipe(
    bufferCount(50)
    // tap(val => {
    //   console.log('CREATED ARRAY');
    //   console.log(val);
    // })
  )
})

export const fetchAmazonMyFeeEstimate$ = (type: string = 'marketplace') => async function fetchAmazonMyFeeEstimate$ ({ props: { authfetch, credentials, asin, amount, currency = 'INR' } }) {
  return ({
    [`${type}FeeEstimate$`]: feeEstimate$(authfetch)({
      'FeesEstimateRequestList.FeesEstimateRequest.1.MarketplaceId': credentials.marketplaceId,
      'FeesEstimateRequestList.FeesEstimateRequest.1.IdType': 'ASIN',
      'FeesEstimateRequestList.FeesEstimateRequest.1.IdValue': asin,
      'FeesEstimateRequestList.FeesEstimateRequest.1.IsAmazonFulfilled': type.toLowerCase() === 'marketplace',
      'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.ListingPrice.Amount': amount,
      'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.ListingPrice.CurrencyCode': currency,
      'FeesEstimateRequestList.FeesEstimateRequest.1.Identifier': new Date().toISOString()
    }).pipe(map(({ GetMyFeesEstimateResponse: { GetMyFeesEstimateResult: { FeesEstimateResultList: { FeesEstimateResult: { FeesEstimate } } } } }) => FeesEstimate))
  })
}

export const fetchAmazonCompetitivePricing$ = async ({ props: { authfetch, credentials, asin } }) => ({
  competitivePricing$: competitivePricing$(authfetch)({
    'MarketplaceId': credentials.marketplaceId,
    'ASINList.ASIN.1': asin
  })
})

export const fetchAmazonLowestOfferListings$ = async ({ props: { authfetch, credentials, asin } }) => ({
  lowestOfferListings$: lowestOfferListings$(authfetch)({
    'MarketplaceId': credentials.marketplaceId,
    'ASINList.ASIN.1': asin,
    'ItemCondition': 'New',
    'ExcludeMe': true
  }).pipe(map(({ GetLowestOfferListingsForASINResponse: { GetLowestOfferListingsForASINResult: { Product } } }) => Product))
})

export const fetchAmazonGetLowestPricedOffers$ = async ({ props: { authfetch, credentials, asin } }) => ({
  lowestPricedOffers$: lowestPricedOffers$(authfetch)({
    'MarketplaceId': credentials.marketplaceId,
    'ASIN': asin,
    'ItemCondition': 'New'
  }).pipe(map(({ GetLowestPricedOffersForASINResponse: { GetLowestPricedOffersForASINResult } }) => GetLowestPricedOffersForASINResult))
})

export const fetchAmazonMatchingProduct$ = async ({ props: { authfetch, credentials, asin } }) => ({
  matchingProduct$: matchingProduct$(authfetch)({
    'MarketplaceId': credentials.marketplaceId,
    'ASINList.ASIN.1': asin
  })
})

export const fetchAmazonProductCategories$ = async ({ props: { authfetch, credentials, asin } }) => ({
  productCategories$: productCategories$(authfetch)({
    'MarketplaceId': credentials.marketplaceId,
    'ASIN': asin
  }).pipe(map(({ GetProductCategoriesForASINResponse: { GetProductCategoriesForASINResult: { Self } } }) => Self))
})

export const fetchOrderItems$ = ({ props: { authfetch, orderListNext$ } }) =>
  ({
    orderItems$: orderListNext$.pipe(
      delay(900),
      concatMap(({ AmazonOrderId }) => orderItems$(authfetch)({ AmazonOrderId }), (order: any, { OrderItems }) => {
        if (Array.isArray(OrderItems.OrderItem)) {
          return OrderItems.OrderItem.map(item => ({ ...order, item }))
        } else {
          return [{ ...order, item: OrderItems.OrderItem, orderItemId: OrderItems.OrderItem.OrderItemId }]
        }
      })
      // tap(res => {
      //   console.log('GOT RESULT');
      //   console.log(res);
      // })
    )
  })

export const fetchOrderList$ = async ({ props: { authfetch, fetchOrderListParams } }) =>
  ({
    orderList$: from(new Promise((resolve, reject) => {
      let operation = retry.operation(retryStrategyShort)

      operation.attempt(function() {
        authfetch.ListOrders(fetchOrderListParams, (error, response) => {
          if (error) {
            if (operation.retry(error)) {
              return
            }
            reject(error)
          } else {
            resolve((parser.parse(response.body)).ListOrdersResponse.ListOrdersResult)
          }
        })
      })
    }))
  })

  export const fetchOrderListNext$ = ({ props: { authfetch, orderList$ } }) =>
  ({
    orderListNext$: orderList$.pipe(
      // tap(console.log),
      // tap(() => { console.log('*****') }),
      expand(({ NextToken }) => NextToken ? orderListNext$(authfetch)(NextToken).pipe(delay(10000)) : empty()),
      concatMap(({ Orders }) => Orders ? Array.isArray(Orders.Order) ? Orders.Order : [Orders.Order] : empty())
      // tap(console.log)
      // toArray()
    )
  })

export const fetchOrderIdsBatch$ = ({ props: { authfetch, orderIdsBatch$ } }) =>
  ({
    orderListNext$: orderIdsBatch$.pipe(
      concatMap((orderIdsBatch: Array<any>) => from(new Promise((resolve, reject) => {
        let operation = retry.operation(retryStrategyShort)

        const rqstIds = orderIdsBatch.reduce((acc, curr, index) => ({ ...acc, [`AmazonOrderId.Id.${index + 1}`]: curr }), {})

        operation.attempt(function() {
          authfetch.GetOrder(rqstIds, (error, response) => {
            if (error) {
              if (operation.retry(error)) {
                return
              }
              reject(error)
            } else {
              resolve((parser.parse(response.body)).GetOrderResponse.GetOrderResult)
            }
          })
        })
      }))),
      // tap(val => {
      //   console.log('Parsed Values');
      //   console.log(val);
      //   // console.log(typeof val.Orders.Order);
      // }),
      concatMap(({ Orders }) => {
        if (Orders) {
          return Array.isArray(Orders.Order) ? Orders.Order : [Orders.Order]
        }
        return empty()
      })
    )
  })

export const getReport$ = ({ props: { authfetch, generatedReportId$ } }) =>
  ({
    report$: generatedReportId$.pipe(concatMap(downloadReport$(authfetch)))
  })

export const getReportById$ = ({ props: { authfetch, reportId } }) => {
  return ({
    report$: downloadReport$(authfetch)(reportId)
  })
}

export const requestReport$ = ({ props: { authfetch, requestReportParams } }) =>
  ({
    requestedReportId$: from(new Promise((resolve, reject) => {
      let operation = retry.operation(retryStrategyShort)

      operation.attempt(function() {
        authfetch.RequestReport(requestReportParams, (error, response) => {
          if (error) {
            if (operation.retry(error)) {
              return
            }
            reject(error)
          } else {
            resolve((parser.parse(response.body, { parseTrueNumberOnly: true })).RequestReportResponse.RequestReportResult.ReportRequestInfo.ReportRequestId)
          }
        })
      })
    }))
  })

  export const requestReportList$ = ({ props: { authfetch, requestReportParams } }) =>
  ({
    reportList$: from(new Promise((resolve, reject) => {
      let operation = retry.operation(retryStrategyShort)

      operation.attempt(function() {
        authfetch.GetReportList(requestReportParams, (error, response) => {
          if (error) {
            if (operation.retry(error)) {
              return
            }
            reject(error)
          } else {
            resolve((parser.parse(response.body, { parseTrueNumberOnly: true })).GetReportListResponse.GetReportListResult)
          }
        })
      })
    }))
  })

  export const requestReportListNext$ = ({ props: { authfetch, reportList$ } }) =>
  ({
    reportListNext$: reportList$.pipe(
      // tap(console.log),
      expand(({ NextToken }) => NextToken ? reportListNext$(authfetch)(NextToken).pipe(delay(10000)) : empty()),
      concatMap(({ ReportInfo }) => ReportInfo ? Array.isArray(ReportInfo) ? ReportInfo : [ReportInfo] : empty())
    )
  })

  export const requestReportResult$ = ({ props: { authfetch, requestedReportId$ } }) =>
  ({
    generatedReportId$: requestedReportId$.pipe(delay(30000), concatMap(reportResult$(authfetch)))
  })

  export const tsv2json$ = async ({ props: { report$, tsvSeperator = '\r\n' } }) =>
({
  json$: report$.pipe(
    concatMap((tsv: string) => {
      // console.log(tsv);
      const arr = tsv.split(tsvSeperator)
      const header = arr[0].split('\t')
      const rows$ = from(arr).pipe(skip(1), map(row => row.split('\t')))
      return rows$.pipe(
        map(row => {
        return row.reduce((rowObj, cell, i) => {
          // @ts-ignore
          rowObj[header[i]] = cell
          return rowObj
        }, {})
      }))
    })
  )
})

export const xml2json$ = async ({ props: { report$ } }) =>
({
  json$: report$.pipe(
    map((val: any) => parser.parse(val))
  )
})

/*
 * ----
 * Subscriptions
 * ----
 */

// export const subscribeJson = async ({ props: { json$ } }) =>
// new Promise((resolve) => {
//   json$.pipe(toArray()).subscribe(json => {
//     resolve({ json })
//   })
// })

// export const subscribeOrderItems = ({ props: { orderItems$ } }) =>
// new Promise((resolve, reject) => {
//   let orderItems = []
//   orderItems$
//     .pipe(
//       toArray(),
//       map(arr => flatten(arr)),
//       catchError(err => Promise.reject(err))
//     )
//     .subscribe({
//       next: result => {
//         // console.log('NEXT CALLED');
//         orderItems = result
//       },
//       complete: () => {
//         // console.log('COMPLETED');
//         resolve({ orderItems })
//       },
//       error: err => {
//         // console.log('CAUGHT ERRROR');
//         // console.log(err);
//         reject(new Error(JSON.stringify(err)))
//       }
//     })
// })

// export const subscribeReport = ({ props: { report$ } }) =>
// new Promise((resolve, reject) => {
//   report$.pipe(catchError(err => Promise.reject(err))).subscribe(response => resolve({ response }), err => reject(new Error(JSON.stringify(err))))
// })

// export const subscribeReportList = ({ props: { reportListNext$ } }) =>
// new Promise((resolve, reject) => {
//   reportListNext$.pipe(toArray(), catchError(err => Promise.reject(err))).subscribe(response => resolve({ response }), err => reject(new Error(JSON.stringify(err))))
// })

/*
* -- Combinator Actions
*/
export const combineAsinProductInfo$ = ({ props: { marketplaceFeeEstimate$, merchantFeeEstimate$, lowestPricedOffers$, lowestOfferListings$, productCategories$ } }) => ({
  productInfo$: forkJoin({
    marketplaceEstimate: marketplaceFeeEstimate$,
    merchantFeeEstimate: merchantFeeEstimate$,
    summary: lowestPricedOffers$,
    offers: lowestOfferListings$,
    category: productCategories$
  })
})

/*
* -- Test Helpers
*/

export const orderIdsObservable = ({ props: { orderIds } }) => ({
  orderIds$: from(orderIds)
})
