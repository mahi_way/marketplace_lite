// https://stackoverflow.com/questions/58149627/best-way-to-get-combination-of-array-of-elements-in-ramda/58149978#58149978
const choose = (n, xs) =>
  n < 1 || n > xs .length
    ? []
    : n === 1
      ? [...xs .map(x => [x])]
      : [
          ...choose(n - 1, xs .slice(1)) .map((ys: Array<any>) => [xs [0], ...ys]),
          ...choose(n , xs .slice(1))
        ]

export const getCombs = (min: number, max: number, xs: Array<any>) =>
  xs .length === 0 || min > max
    ? []
    : [...choose(min, xs), ...getCombs(min + 1, max, xs)]
