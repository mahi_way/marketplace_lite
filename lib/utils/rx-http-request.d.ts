import { RequestAPI, Request, CoreOptions, RequiredUriUrl, RequestResponse } from 'request'
import { Observable } from 'rxjs'
import { Cookie, RxCookieJar } from './rx-cookie-jar'
/**
 * Class definition
 */
export declare class RxHttpRequest {
  private static _instance
  private readonly _request
  /**
   * Returns singleton instance
   *
   * @return {RxHttpRequest}
   */
  static instance(): RxHttpRequest
  /**
   * Class constructor
   */
  constructor(req: RequestAPI<Request, CoreOptions, RequiredUriUrl>)
  /**
   * Returns private attribute _request
   *
   * @return {RequestAPI<Request, CoreOptions, RequiredUriUrl>}
   */
  get request(): RequestAPI<Request, CoreOptions, RequiredUriUrl>
  /**
   * This method returns a wrapper around the normal rx-http-request API that defaults to whatever options
   * you pass to it.
   * It does not modify the global rx-http-request API; instead, it returns a wrapper that has your default settings
   * applied to it.
   * You can _call .defaults() on the wrapper that is returned from rx-http-request.defaults to add/override defaults
   * that were previously defaulted.
   *
   * @param options
   *
   * @return {RxHttpRequest}
   */
  defaults(options: CoreOptions): RxHttpRequest
  /**
   * Function to do a GET HTTP request
   *
   * @param uri {string}
   * @param options {CoreOptions}
   *
   * @return {Observable<RxHttpRequestResponse<R>>}
   */
  get<R = any>(uri: string, options?: CoreOptions): Observable<RxHttpRequestResponse<R>>
  /**
   * Function to do a GET HTTP request and to return a buffer
   *
   * @param uri
   * @param options
   *
   * @return {Observable<RxHttpRequestResponse<Buffer>>}
   */
  getBuffer<Buffer>(uri: string, options?: CoreOptions): Observable<RxHttpRequestResponse<Buffer>>
  /**
   * Function to do a POST HTTP request
   *
   * @param uri {string}
   * @param options {CoreOptions}
   *
   * @return {Observable<RxHttpRequestResponse<R>>}
   */
  post<R = any>(uri: string, options?: CoreOptions): Observable<RxHttpRequestResponse<R>>
  /**
   * Function to do a PUT HTTP request
   *
   * @param uri {string}
   * @param options {CoreOptions}
   *
   * @return {Observable<RxHttpRequestResponse<R>>}
   */
  put<R = any>(uri: string, options?: CoreOptions): Observable<RxHttpRequestResponse<R>>
  /**
   * Function to do a PATCH HTTP request
   *
   * @param uri {string}
   * @param options {CoreOptions}
   *
   * @return {Observable<RxHttpRequestResponse<R>>}
   */
  patch<R = any>(uri: string, options?: CoreOptions): Observable<RxHttpRequestResponse<R>>
  /**
   * Function to do a DELETE HTTP request
   *
   * @param uri {string}
   * @param options {CoreOptions}
   *
   * @return {Observable<RxHttpRequestResponse<R>>}
   */
  delete<R = any>(uri: string, options?: CoreOptions): Observable<RxHttpRequestResponse<R>>
  /**
   * Function to do a HEAD HTTP request
   *
   * @param uri {string}
   * @param options {CoreOptions}
   *
   * @return {Observable<RxHttpRequestResponse<R>>}
   */
  head<R = any>(uri: string, options?: CoreOptions): Observable<RxHttpRequestResponse<R>>
  /**
   * Function to do a OPTIONS HTTP request
   *
   * @param uri {string}
   * @param options {CoreOptions}
   *
   * @return {Observable<RxHttpRequestResponse<R>>}
   */
  options<R = any>(uri: string, options?: CoreOptions): Observable<RxHttpRequestResponse<R>>
  /**
   * Function that creates a new rx cookie jar
   *
   * @return {Observable<RxCookieJar>}
   */
  jar(): Observable<RxCookieJar>
  /**
   * Function that creates a new cookie
   *
   * @param str {string}
   *
   * @return {Observable<Cookie>}
   */
  cookie(str: string): Observable<Cookie>
  /**
   * Function to do a HTTP request for given method
   *
   * @param method {string}
   * @param uri {string}
   * @param options {CoreOptions}
   *
   * @return {Observable<RxHttpRequestResponse<R>>}
   *
   * @private
   */
  private _call
  /**
   * Function to check existing function in request API passed in parameter for a new instance
   *
   * @param req {RequestAPI<Request, CoreOptions, RequiredUriUrl>}
   *
   * @private
   */
  private _checkRequestParam
}
/**
 * Export {RxHttpRequest} instance
 */
export declare const RxHR: RxHttpRequest
/**
 * Export response interface
 */
export interface RxHttpRequestResponse<R = any> {
  response: RequestResponse
  body: R
}
/**
 * Export all initial elements
 */
export { RequestAPI, Request, CoreOptions, RequiredUriUrl, RequestResponse }
