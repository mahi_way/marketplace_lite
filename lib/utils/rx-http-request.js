"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var buffer_1 = require("buffer");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var rx_cookie_jar_1 = require("./rx-cookie-jar");
var request = require('request');
/**
 * Class definition
 */
var RxHttpRequest = /** @class */ (function () {
    /**
     * Class constructor
     */
    function RxHttpRequest(req) {
        // check request parameter
        this._checkRequestParam(req);
        // set request object
        this._request = req;
    }
    /**
     * Returns singleton instance
     *
     * @return {RxHttpRequest}
     */
    RxHttpRequest.instance = function () {
        if (!(RxHttpRequest._instance instanceof RxHttpRequest)) {
            RxHttpRequest._instance = new RxHttpRequest(request);
        }
        return RxHttpRequest._instance;
    };
    Object.defineProperty(RxHttpRequest.prototype, "request", {
        /**
         * Returns private attribute _request
         *
         * @return {RequestAPI<Request, CoreOptions, RequiredUriUrl>}
         */
        get: function () {
            return this._request;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * This method returns a wrapper around the normal rx-http-request API that defaults to whatever options
     * you pass to it.
     * It does not modify the global rx-http-request API; instead, it returns a wrapper that has your default settings
     * applied to it.
     * You can _call .defaults() on the wrapper that is returned from rx-http-request.defaults to add/override defaults
     * that were previously defaulted.
     *
     * @param options
     *
     * @return {RxHttpRequest}
     */
    RxHttpRequest.prototype.defaults = function (options) {
        return new RxHttpRequest(this._request.defaults(options));
    };
    /**
     * Function to do a GET HTTP request
     *
     * @param uri {string}
     * @param options {CoreOptions}
     *
     * @return {Observable<RxHttpRequestResponse<R>>}
     */
    RxHttpRequest.prototype.get = function (uri, options) {
        return (this._call('get', uri, Object.assign({}, options || {})));
    };
    /**
     * Function to do a GET HTTP request and to return a buffer
     *
     * @param uri
     * @param options
     *
     * @return {Observable<RxHttpRequestResponse<Buffer>>}
     */
    RxHttpRequest.prototype.getBuffer = function (uri, options) {
        var _this = this;
        return new rxjs_1.Observable(function (observer) {
            try {
                _this._request
                    .get(uri, Object.assign({}, options || {}))
                    .on('response', function (response) {
                    var res;
                    // @ts-ignore
                    response.on('data', function (data) { return (res = res ? buffer_1.Buffer.concat([].concat(res, data)) : data); });
                    response.on('end', function () {
                        observer.next(Object.assign({}, {
                            response: response,
                            body: res
                        }));
                        observer.complete();
                    });
                })
                    .on('error', /* istanbul ignore next */ function (/* istanbul ignore next */ error) { return observer.error(error); });
            }
            catch (error) {
                observer.error(error);
            }
        });
    };
    /**
     * Function to do a POST HTTP request
     *
     * @param uri {string}
     * @param options {CoreOptions}
     *
     * @return {Observable<RxHttpRequestResponse<R>>}
     */
    RxHttpRequest.prototype.post = function (uri, options) {
        return (this._call('post', uri, Object.assign({}, options || {})));
    };
    /**
     * Function to do a PUT HTTP request
     *
     * @param uri {string}
     * @param options {CoreOptions}
     *
     * @return {Observable<RxHttpRequestResponse<R>>}
     */
    RxHttpRequest.prototype.put = function (uri, options) {
        return (this._call('put', uri, Object.assign({}, options || {})));
    };
    /**
     * Function to do a PATCH HTTP request
     *
     * @param uri {string}
     * @param options {CoreOptions}
     *
     * @return {Observable<RxHttpRequestResponse<R>>}
     */
    RxHttpRequest.prototype.patch = function (uri, options) {
        return (this._call('patch', uri, Object.assign({}, options || {})));
    };
    /**
     * Function to do a DELETE HTTP request
     *
     * @param uri {string}
     * @param options {CoreOptions}
     *
     * @return {Observable<RxHttpRequestResponse<R>>}
     */
    RxHttpRequest.prototype.delete = function (uri, options) {
        return (this._call('del', uri, Object.assign({}, options || {})));
    };
    /**
     * Function to do a HEAD HTTP request
     *
     * @param uri {string}
     * @param options {CoreOptions}
     *
     * @return {Observable<RxHttpRequestResponse<R>>}
     */
    RxHttpRequest.prototype.head = function (uri, options) {
        return (this._call('head', uri, Object.assign({}, options || {})));
    };
    /**
     * Function to do a OPTIONS HTTP request
     *
     * @param uri {string}
     * @param options {CoreOptions}
     *
     * @return {Observable<RxHttpRequestResponse<R>>}
     */
    RxHttpRequest.prototype.options = function (uri, options) {
        return (this._call('options', uri, Object.assign({}, options || {})));
    };
    /**
     * Function that creates a new rx cookie jar
     *
     * @return {Observable<RxCookieJar>}
     */
    RxHttpRequest.prototype.jar = function () {
        return rxjs_1.of(new rx_cookie_jar_1.RxCookieJar(this._request.jar()));
    };
    /**
     * Function that creates a new cookie
     *
     * @param str {string}
     *
     * @return {Observable<Cookie>}
     */
    RxHttpRequest.prototype.cookie = function (str) {
        return rxjs_1.of(this._request.cookie(str));
    };
    /**
     * Function to do a HTTP request for given method
     *
     * @param method {string}
     * @param uri {string}
     * @param options {CoreOptions}
     *
     * @return {Observable<RxHttpRequestResponse<R>>}
     *
     * @private
     */
    RxHttpRequest.prototype._call = function (method, uri, options) {
        var _this = this;
        return new rxjs_1.Observable(function (observer) {
            rxjs_1.of([].concat(uri, Object.assign({}, options || {}), (function (error, response, body) {
                rxjs_1.of(rxjs_1.of(error))
                    .pipe(operators_1.flatMap(function (obsError) {
                    return rxjs_1.merge(obsError.pipe(operators_1.filter(function (_) { return !!_; }), operators_1.tap(function (err) { return observer.error(err); })), obsError.pipe(operators_1.filter(function (_) { return !_; }), operators_1.flatMap(function () {
                        return !!response
                            ? rxjs_1.of(response)
                            : rxjs_1.throwError(new Error('No response found'));
                    }), operators_1.flatMap(function (_) {
                        return rxjs_1.of({
                            response: _,
                            body: body
                        });
                    }), operators_1.tap(function (_) { return observer.next(_); }), operators_1.tap(function () { return observer.complete(); })));
                }))
                    .subscribe(function () { return undefined; }, function (err) { return observer.error(err); });
            })))
                .pipe(operators_1.map(function (_) {
                return _this._request[method].apply(_this._request, _);
            }))
                .subscribe(function () { return undefined; }, function (err) { return observer.error(err); });
        });
    };
    /**
     * Function to check existing function in request API passed in parameter for a new instance
     *
     * @param req {RequestAPI<Request, CoreOptions, RequiredUriUrl>}
     *
     * @private
     */
    RxHttpRequest.prototype._checkRequestParam = function (req) {
        // check existing function in API
        if (!req ||
            Object.prototype.toString.call(req.get) !== '[object Function]' ||
            Object.prototype.toString.call(req.head) !== '[object Function]' ||
            Object.prototype.toString.call(req.post) !== '[object Function]' ||
            Object.prototype.toString.call(req.put) !== '[object Function]' ||
            Object.prototype.toString.call(req.patch) !== '[object Function]' ||
            Object.prototype.toString.call(req.del) !== '[object Function]' ||
            Object.prototype.toString.call(req.defaults) !== '[object Function]' ||
            Object.prototype.toString.call(req.jar) !== '[object Function]' ||
            Object.prototype.toString.call(req.cookie) !== '[object Function]') {
            throw new TypeError('Parameter must be a valid `request` module API');
        }
    };
    return RxHttpRequest;
}());
exports.RxHttpRequest = RxHttpRequest;
/**
 * Export {RxHttpRequest} instance
 */
exports.RxHR = RxHttpRequest.instance();
