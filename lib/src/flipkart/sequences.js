"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var function_tree_1 = require("function-tree");
var actions_1 = require("./actions");
// export const Sequences = 'FLIPKART_SELLER_API_FUNCTION_TREE_SEQUENCES'
exports.fetchFlipkartSingleOrder = function_tree_1.sequence('Fetch Flipkart Single Order By OrderItemId', [
    actions_1.createHeader,
    actions_1.fetchSingleOrderItem
]);
exports.fetchFlipkartOrders = function_tree_1.sequence('Fetch Flipkart Orders', [
    actions_1.createHeader,
    actions_1.fetchShipments,
    actions_1.mapShipments,
    actions_1.fetchOrderItem
    // subscribeOrderItems
]);
exports.fetchFlipkartReturns = function_tree_1.sequence('Fetch Flipkart Returns', [
    actions_1.createHeader,
    actions_1.fetchReturns
    // subscribeReturns
]);
