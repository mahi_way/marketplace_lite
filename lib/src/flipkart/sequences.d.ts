export declare const fetchFlipkartSingleOrder: () => void
export declare const fetchFlipkartOrders: () => void
export declare const fetchFlipkartReturns: () => void
