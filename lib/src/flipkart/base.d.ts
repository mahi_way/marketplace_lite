export declare const fetchOrderByOrderItemIds$: (headers: any) => (id: any) => import('rxjs').Observable<any>
export declare const fetchPreShipments$: (
  headers: any
) => ({ from, to }: { from: any; to: any }) => import('rxjs').Observable<unknown>
export declare const fetchPostShipments$: (
  headers: any
) => ({ from, to }: { from: any; to: any }) => import('rxjs').Observable<unknown>
export declare const fetchCancelledShipments$: (
  headers: any
) => ({ from, to }: { from: any; to: any }) => import('rxjs').Observable<unknown>
export declare const fetchReturns$: (
  headers: any
) => ({ type, date }: { type: any; date: any }) => import('rxjs').Observable<unknown>
export declare const parseHeader: (credentials: any) => any
