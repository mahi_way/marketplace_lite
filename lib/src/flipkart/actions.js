"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var async_retry_1 = require("async-retry");
var axios_1 = require("axios");
var base_1 = require("./base");
exports.createHeader = function (_a) {
    var credentials = _a.props.credentials;
    return ({
        headers: base_1.parseHeader(credentials)
    });
};
exports.checkFlipkartLogin = function (_a) {
    var _b = _a.props, headers = _b.headers, locationList = _b.locationList, path = _a.path;
    return __awaiter(void 0, void 0, void 0, function () {
        var header, result, err_1;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    if (locationList) {
                        return [2 /*return*/, path.valid()];
                    }
                    header = {
                        Accept: headers.Accept,
                        'Accept-Language': headers['Accept-Language'],
                        Connection: headers.Connection,
                        Cookie: headers.Cookie,
                        Host: 'seller.flipkart.com',
                        Referer: 'https://seller.flipkart.com/index.html',
                        'Sec-Fetch-Mode': 'cors',
                        'Sec-Fetch-Site': 'same-origin',
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
                        'X-Requested-With': 'XMLHttpRequest'
                    };
                    _c.label = 1;
                case 1:
                    _c.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, async_retry_1.default(function () { return __awaiter(void 0, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, axios_1.default.request({
                                            url: "https://seller.flipkart.com/napi/get-locations?locationType=pickup&include=state&capabilities=NON_FBF%2CFBF_LITE",
                                            headers: header
                                        })];
                                    case 1:
                                        result = (_a.sent()).data;
                                        return [2 /*return*/];
                                }
                            });
                        }); }, {
                            randomize: true,
                            onRetry: function (err) {
                                // tslint:disable-next-line: no-console
                                console.log('RETRYING FLIPKART GETLOCATION');
                                // tslint:disable-next-line: no-console
                                console.log(err);
                            }
                        })];
                case 2:
                    _c.sent();
                    return [3 /*break*/, 4];
                case 3:
                    err_1 = _c.sent();
                    // return path.invalid({ error: err });
                    throw new Error(err_1);
                case 4:
                    if (typeof result === 'string') {
                        return [2 /*return*/, path.invalid()];
                    }
                    return [2 /*return*/, path.valid({
                            locationList: (result && result.result && result.result.multiLocationList) || []
                        })];
            }
        });
    });
};
exports.fetchSingleOrderItem = function (_a) {
    var _b = _a.props, headers = _b.headers, orderId = _b.orderId;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    orderItems$: base_1.fetchOrderByOrderItemIds$(headers)(orderId)
                })];
        });
    });
};
exports.fetchOrderItem = function (_a) {
    var _b = _a.props, headers = _b.headers, mappedShipments$ = _b.mappedShipments$;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    orderItems$: mappedShipments$.pipe(operators_1.concatMap(function (_a) {
                        var orderItemId = _a.orderItemId;
                        return base_1.fetchOrderByOrderItemIds$(headers)(orderItemId);
                    }, function (order, orderItems) { return (__assign(__assign({}, order), { item: orderItems[0] })); }))
                })];
        });
    });
};
exports.fetchShipments = function (_a) {
    var _b = _a.props, headers = _b.headers, params = _b.params;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    shipments$: rxjs_1.merge(base_1.fetchPreShipments$(headers)(params), base_1.fetchPostShipments$(headers)(params), base_1.fetchCancelledShipments$(headers)(params))
                })];
        });
    });
};
exports.fetchReturns = function (_a) {
    var _b = _a.props, headers = _b.headers, date = _b.params.date;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    returns$: rxjs_1.merge(base_1.fetchReturns$(headers)({ type: 'customer_return', date: date }), base_1.fetchReturns$(headers)({ type: 'courier_return', date: date }))
                })];
        });
    });
};
exports.mapShipments = function (_a) {
    var shipments$ = _a.props.shipments$;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_b) {
            return [2 /*return*/, ({
                    mappedShipments$: shipments$.pipe(operators_1.concatMap(function (_a) {
                        var orderItems = _a.orderItems, subShipments = _a.subShipments, rest = __rest(_a, ["orderItems", "subShipments"]);
                        return orderItems.map(function (order, index) { return (__assign(__assign({}, rest), { orderItemId: order.orderItemId, order: order, subShipments: subShipments[index] })); });
                    }))
                })
                // export const subscribeOrderItems = async ({ props: { orderItems$ } }: any) =>
                //   new Promise(resolve => {
                //     orderItems$
                //       .pipe(
                //         toArray()
                //         // catchError(err => {
                //         //   console.log('*****');
                //         //   console.log(err);
                //         //   return empty();
                //         // })
                //       )
                //       .subscribe(orderItems => {
                //         resolve({ orderItems })
                //       })
                //   })
                // export const subscribeReturns = async ({ props: { returns$ } }: any) =>
                //   new Promise(resolve => {
                //     returns$.pipe(toArray()).subscribe(returns => {
                //       resolve({ returns })
                //     })
                //   })
            ];
        });
    });
};
// export const subscribeOrderItems = async ({ props: { orderItems$ } }: any) =>
//   new Promise(resolve => {
//     orderItems$
//       .pipe(
//         toArray()
//         // catchError(err => {
//         //   console.log('*****');
//         //   console.log(err);
//         //   return empty();
//         // })
//       )
//       .subscribe(orderItems => {
//         resolve({ orderItems })
//       })
//   })
// export const subscribeReturns = async ({ props: { returns$ } }: any) =>
//   new Promise(resolve => {
//     returns$.pipe(toArray()).subscribe(returns => {
//       resolve({ returns })
//     })
//   })
