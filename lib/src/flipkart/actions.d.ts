export declare const createHeader: ({
  props: { credentials }
}: any) => {
  headers: any
}
export declare const checkFlipkartLogin: ({
  props: { headers, locationList },
  path
}: {
  props: {
    headers: any
    locationList: any
  }
  path: any
}) => Promise<any>
export declare const fetchSingleOrderItem: ({
  props: { headers, orderId }
}: any) => Promise<{
  orderItems$: import('rxjs').Observable<any>
}>
export declare const fetchOrderItem: ({
  props: { headers, mappedShipments$ }
}: any) => Promise<{
  orderItems$: any
}>
export declare const fetchShipments: ({
  props: { headers, params }
}: any) => Promise<{
  shipments$: import('rxjs').Observable<unknown>
}>
export declare const fetchReturns: ({
  props: {
    headers,
    params: { date }
  }
}: any) => Promise<{
  returns$: import('rxjs').Observable<unknown>
}>
export declare const mapShipments: ({
  props: { shipments$ }
}: any) => Promise<{
  mappedShipments$: any
}>
