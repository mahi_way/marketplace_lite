"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var ramda_1 = require("ramda");
var rx_http_request_1 = require("../../utils/rx-http-request");
/*
 * ---
 * Constants
 * ---
 */
var apiurl = 'https://api.flipkart.net/sellers';
/*
 * ----
 * Helpers
 * ----
 */
var constructHeader = function (_a) {
    var accesskey = _a.accesskey, secret = _a.secret, bearer = _a.bearer;
    return ({
        'Content-Type': 'application/vnd.slc+json',
        Accept: 'application/json',
        appid: accesskey,
        'app-secret': secret,
        Authorization: "bearer " + bearer
    });
};
var getRequest$ = function (headers, url) { return rx_http_request_1.RxHR.get(url, {
    headers: headers,
    json: true
}).pipe(
// tap(console.log),
operators_1.map(function (data) { return data.response; }), operators_1.filter(function (response) { return response.statusCode === 200; }), operators_1.map(function (response) { return response.body; })); };
var postRequest$ = function (headers, url, body) { return rx_http_request_1.RxHR.post(url, {
    headers: headers,
    json: true,
    body: body
}).pipe(operators_1.map(function (data) { return data.response; }), operators_1.filter(function (response) { return response.statusCode === 200; }), operators_1.map(function (response) { return response.body; })); };
// export const fetchOrdersByOrderItemIds = headers => id => fetchURL({
//   url: `${apiurl}/v2/orders?orderItemIds=${id}`,
//   headers
// });
/*
 * ----
 * Exports
 * ----
 */
exports.fetchOrderByOrderItemIds$ = function (headers) { return function (id) { return getRequest$(headers, apiurl + "/v2/orders?orderItemIds=" + id).pipe(operators_1.map(function (response) { return response.orderItems ? Array.isArray(response.orderItems) ? response.orderItems : [response.orderItems] : []; })); }; };
exports.fetchPreShipments$ = function (headers) { return function (_a) {
    var from = _a.from, to = _a.to;
    return postRequest$(headers, apiurl + "/v3/shipments/filter", {
        filter: {
            type: 'preDispatch',
            // serviceProfiles: 'FBF_LITE',
            states: ['APPROVED', 'PACKING_IN_PROGRESS', 'PACKED', 'FORM_FAILED', 'READY_TO_DISPATCH'],
            orderDate: {
                from: from,
                to: to
            }
        }
    }).pipe(operators_1.expand(function (_a) {
        var hasMore = _a.hasMore, nextPageUrl = _a.nextPageUrl;
        return hasMore ? getRequest$(headers, "" + apiurl + nextPageUrl) : rxjs_1.empty();
    }), operators_1.concatMap(function (_a) {
        var shipments = _a.shipments;
        return shipments;
    }));
}; };
exports.fetchPostShipments$ = function (headers) { return function (_a) {
    var from = _a.from, to = _a.to;
    return postRequest$(headers, apiurl + "/v3/shipments/filter", {
        filter: {
            type: 'postDispatch',
            // serviceProfiles: 'FBF_LITE',
            states: ['SHIPPED', 'DELIVERED'],
            orderDate: {
                from: from,
                to: to
            }
        }
    }).pipe(operators_1.expand(function (_a) {
        var hasMore = _a.hasMore, nextPageUrl = _a.nextPageUrl;
        return hasMore ? getRequest$(headers, "" + apiurl + nextPageUrl) : rxjs_1.empty();
    }), operators_1.concatMap(function (_a) {
        var shipments = _a.shipments;
        return shipments;
    }));
}; };
exports.fetchCancelledShipments$ = function (headers) { return function (_a) {
    var from = _a.from, to = _a.to;
    return postRequest$(headers, apiurl + "/v3/shipments/filter", {
        filter: {
            type: 'cancelled',
            states: ['CANCELLED'],
            // serviceProfiles: 'FBF_LITE',
            cancellationType: 'buyerCancellation',
            orderDate: {
                from: from,
                to: to
            }
        }
    }).pipe(operators_1.expand(function (_a) {
        var hasMore = _a.hasMore, nextPageUrl = _a.nextPageUrl;
        return hasMore ? getRequest$(headers, "" + apiurl + nextPageUrl) : rxjs_1.empty();
    }), operators_1.concatMap(function (_a) {
        var shipments = _a.shipments;
        return shipments;
    }));
}; };
exports.fetchReturns$ = function (headers) { return function (_a) {
    var type = _a.type, date = _a.date;
    return getRequest$(headers, apiurl + "/v2/returns?source=" + type + "&createdAfter=" + date).pipe(operators_1.expand(function (_a) {
        var hasMore = _a.hasMore, nextPageUrl = _a.nextPageUrl;
        return hasMore ? getRequest$(headers, "" + apiurl + nextPageUrl) : rxjs_1.empty();
    }), operators_1.concatMap(function (_a) {
        var returnItems = _a.returnItems;
        return returnItems;
    }));
}; };
exports.parseHeader = function (credentials) {
    return ramda_1.compose(constructHeader, ramda_1.pick(['accesskey', 'secret', 'bearer']))(credentials);
};
