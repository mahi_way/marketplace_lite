"use strict";
/**
 * Testing Docs build
 *
 * @since 1.0.0
 */
Object.defineProperty(exports, "__esModule", { value: true });
function sum(a, b) {
    return a + b;
}
exports.sum = sum;
