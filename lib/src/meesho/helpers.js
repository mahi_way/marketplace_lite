"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var axios_1 = require("axios");
var SlaStatus;
(function (SlaStatus) {
    SlaStatus["breached"] = "breached";
    SlaStatus["sla_others"] = "sla_others";
    SlaStatus["breaching_soon"] = "breaching_soon";
})(SlaStatus = exports.SlaStatus || (exports.SlaStatus = {}));
var SortOrder;
(function (SortOrder) {
    SortOrder["ASC"] = "ASC";
    SortOrder["DESC"] = "DESC";
})(SortOrder = exports.SortOrder || (exports.SortOrder = {}));
// -------------------------------------------------------------------
/**
 * Fetch Meesho Orders
 *
 * @param status
 *
 * @returns Observable
 */
function fetchOrders(_a) {
    var url = _a.url, _b = _a.status, status = _b === void 0 ? 0 : _b, cookie = _a.cookie, identifier = _a.identifier, supplierId = _a.supplierId, _c = _a.filter, filter = _c === void 0 ? null : _c;
    var axiosConfig = {
        url: url,
        method: 'post',
        headers: {
            'authority': 'supplier.meeshosupply.com',
            'method': 'post',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'scheme': 'https',
            'accept': 'application/json',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json;charset=UTF-8',
            'cookie': cookie,
            'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
            'origin': 'https://supplier.meeshosupply.com',
            'referer': 'https://supplier.meeshosupply.com/ovifl/orders',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        },
        data: filter ? {
            identifier: identifier,
            supplier_id: supplierId,
            status: status,
            filter: filter
        } : {
            identifier: identifier,
            supplier_id: supplierId,
            status: status
        }
    };
    return rxjs_1.from(axios_1.default.request(axiosConfig)).pipe(operators_1.filter(function (response) { return response.status === 200; }), operators_1.map(function (response) { return response.data; }));
}
exports.fetchOrders = fetchOrders;
/**
 * Fetch Meesho Returns
 *
 * @param status
 * @returns Observable
 */
function fetchReturns(_a) {
    var _b = _a.pagingToken, pagingToken = _b === void 0 ? null : _b, cookie = _a.cookie, identifier = _a.identifier, supplierId = _a.supplierId, _c = _a.filter, filter = _c === void 0 ? null : _c, _d = _a.size, size = _d === void 0 ? 100 : _d, _e = _a.sortOrder, sortOrder = _e === void 0 ? SortOrder.DESC : _e;
    var data = {
        identifier: identifier,
        supplier_id: supplierId,
        size: size,
        sort_order: sortOrder
    };
    if (filter) {
        data.filter = filter;
    }
    if (pagingToken) {
        data.__paging_token = pagingToken;
    }
    var axiosConfig = {
        url: 'https://supplier.meeshosupply.com/api/fetch/returned/subOrders',
        method: 'post',
        headers: {
            'authority': 'supplier.meeshosupply.com',
            'method': 'post',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'scheme': 'https',
            'accept': 'application/json',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json;charset=UTF-8',
            'cookie': cookie,
            'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
            'origin': 'https://supplier.meeshosupply.com',
            'referer': 'https://supplier.meeshosupply.com/ovifl/orders/returned',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        },
        data: data
    };
    return rxjs_1.from(axios_1.default.request(axiosConfig)).pipe(operators_1.filter(function (response) { return response.status === 200; }), operators_1.map(function (response) { return response.data; }));
}
exports.fetchReturns = fetchReturns;
function fetchPenalties(_a) {
    var cookie = _a.cookie, identifier = _a.identifier, _b = _a.size, size = _b === void 0 ? "10" : _b, _c = _a.offset, offset = _c === void 0 ? "0" : _c;
    var axiosConfig = {
        url: 'https://supplier.meeshosupply.com/api/payments-new/penalties',
        method: 'post',
        headers: {
            'authority': 'supplier.meeshosupply.com',
            'method': 'post',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'scheme': 'https',
            'accept': 'application/json',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json;charset=UTF-8',
            'cookie': cookie,
            'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
            'origin': 'https://supplier.meeshosupply.com',
            'referer': 'https://supplier.meeshosupply.com/ovifl/payment/penalties',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        },
        data: { "supplierIdentifier": identifier, "limit": size, "offset": offset }
    };
    return rxjs_1.from(axios_1.default.request(axiosConfig)).pipe(operators_1.filter(function (response) { return response.status === 200; }), operators_1.map(function (response) { return response.data; }));
}
exports.fetchPenalties = fetchPenalties;
function fetchInvoices(_a) {
    var cookie = _a.cookie, identifier = _a.identifier;
    var axiosConfig = {
        url: 'https://supplier.meeshosupply.com/api/payments/invoices',
        method: 'post',
        headers: {
            'authority': 'supplier.meeshosupply.com',
            'method': 'post',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'scheme': 'https',
            'accept': 'application/json',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json;charset=UTF-8',
            'cookie': cookie,
            'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
            'origin': 'https://supplier.meeshosupply.com',
            'referer': 'https://supplier.meeshosupply.com/ovifl/payment/previous-invoices',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        },
        data: { 'identifier': identifier }
    };
    return rxjs_1.from(axios_1.default.request(axiosConfig)).pipe(operators_1.filter(function (response) { return response.status === 200; }), operators_1.map(function (response) { return response.data; }));
}
exports.fetchInvoices = fetchInvoices;
function fetchOutstandingPreviousPayments(_a) {
    var action = _a.action, status = _a.status, identifier = _a.identifier, cookie = _a.cookie;
    var axiosConfig = {
        url: 'https://supplier.meeshosupply.com/api/payments-new/all',
        method: 'post',
        headers: {
            'authority': 'supplier.meeshosupply.com',
            'method': 'post',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'scheme': 'https',
            'accept': 'application/json',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json;charset=UTF-8',
            'cookie': cookie,
            'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
            'origin': 'https://supplier.meeshosupply.com',
            'referer': 'https://supplier.meeshosupply.com/ovifl/payment/' + action,
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        },
        data: { "identifier": identifier, "status": status }
    };
    return rxjs_1.from(axios_1.default.request(axiosConfig)).pipe(operators_1.filter(function (response) { return response.status === 200; }), operators_1.map(function (response) { return response.data; }));
}
exports.fetchOutstandingPreviousPayments = fetchOutstandingPreviousPayments;
function fetchPayments(_a) {
    var cookie = _a.cookie, identifier = _a.identifier, supplierId = _a.supplierId, typeCall = _a.typeCall, date = _a.date, status = _a.status;
    var axiosConfig = {
        url: 'https://supplier.meeshosupply.com/api/payment-new/payment-details',
        method: 'post',
        headers: {
            'authority': 'supplier.meeshosupply.com',
            'method': 'post',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'scheme': 'https',
            'accept': 'application/json',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json;charset=UTF-8',
            'cookie': cookie,
            'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
            'origin': 'https://supplier.meeshosupply.com',
            'referer': 'https://supplier.meeshosupply.com/api/payment-new/payment-details' + typeCall + date,
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        },
        data: { "identifier": identifier, "supplier_id": supplierId, "status": status, "limit": 1000, "date": date }
    };
    return rxjs_1.from(axios_1.default.request(axiosConfig)).pipe(operators_1.filter(function (response) { return response.status === 200; }), operators_1.map(function (response) { return response.data; }));
}
exports.fetchPayments = fetchPayments;
function fetchOrderShippedUrl(_a) {
    var cookie = _a.cookie, identifier = _a.identifier, min = _a.min, max = _a.max;
    var axiosConfig = {
        url: 'https://supplier.meeshosupply.com/api/order/gst/details',
        method: 'post',
        headers: {
            'authority': 'supplier.meeshosupply.com',
            'method': 'POST',
            'path': '/api/order/gst/details',
            'scheme': 'https',
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'content-type': 'application/json;charset=UTF-8',
            'cookie': cookie,
            'if-modified-since': 'Mon, 26 Jul 1997 05:00:00 GMT',
            'origin': 'https://supplier.meeshosupply.com',
            'pragma': 'no-cache',
            'referer': 'https://supplier.meeshosupply.com/ovifl/orders',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
        },
        data: { "identifier": identifier,
            "order_date": {
                'min': min,
                'max': max,
            }
        }
    };
    return rxjs_1.from(axios_1.default.request(axiosConfig)).pipe(operators_1.filter(function (response) { return response.status === 200; }), operators_1.map(function (response) { return response.data.data.invoice_url; }));
}
exports.fetchOrderShippedUrl = fetchOrderShippedUrl;
