import { Observable } from 'rxjs'
export declare enum SlaStatus {
  breached = 'breached',
  sla_others = 'sla_others',
  breaching_soon = 'breaching_soon'
}
export declare enum SortOrder {
  ASC = 'ASC',
  DESC = 'DESC'
}
export interface OrderFilters {
  order_date: {
    min: string
    max: string
  }
  sla_status: [SlaStatus]
}
export interface ReturnFilters {
  pickup_date: {
    min: string
    max: string
  }
  delivered_date: {
    min: string
    max: string
  }
}
interface FetchOrdersConfig {
  url: string
  status?: number
  cookie: string
  identifier: string
  supplierId: number
  filter?: OrderFilters
}
interface FetchReturnsConfig {
  cookie: string
  identifier: string
  supplierId: number
  filter?: ReturnFilters
  pagingToken?: string
  size?: number
  sortOrder?: SortOrder
}
interface FetchPenaltiesConfig {
  cookie: string
  identifier: string
  size?: string
  offset?: string
}
export interface ReturnResponse {
  paging: {
    next: string
  }
  returned_suborder_data_list: Array<object>
}
interface FetchOutPrevPaymentsConfig {
  action: string
  status: string
  identifier: string
  cookie: string
}
interface FetchPaymentsConfig {
  cookie: string
  identifier: string
  supplierId: string
  typeCall: string
  date: string
  status: string
}
/**
 * Fetch Meesho Orders
 *
 * @param status
 *
 * @returns Observable
 */
export declare function fetchOrders({
  url,
  status,
  cookie,
  identifier,
  supplierId,
  filter
}: FetchOrdersConfig): Observable<any>
export interface ReturnsData {
  identifier: string
  supplier_id: number
  filter?: ReturnFilters
  size: number
  sort_order: string
  __paging_token?: string
}
/**
 * Fetch Meesho Returns
 *
 * @param status
 * @returns Observable
 */
export declare function fetchReturns({
  pagingToken,
  cookie,
  identifier,
  supplierId,
  filter,
  size,
  sortOrder
}: FetchReturnsConfig): Observable<any>
export declare function fetchPenalties({ cookie, identifier, size, offset }: FetchPenaltiesConfig): Observable<any>
export declare function fetchInvoices({ cookie, identifier }: { cookie: string; identifier: string }): Observable<any>
export declare function fetchOutstandingPreviousPayments({
  action,
  status,
  identifier,
  cookie
}: FetchOutPrevPaymentsConfig): Observable<any>
export declare function fetchPayments({
  cookie,
  identifier,
  supplierId,
  typeCall,
  date,
  status
}: FetchPaymentsConfig): Observable<any>
export declare function fetchOrderShippedUrl({
  cookie,
  identifier,
  min,
  max
}: {
  cookie: string
  identifier: string
  min: string
  max: string
}): Observable<any>
export {}
