import { OrderFilters, ReturnFilters, SortOrder } from './helpers'
/**
 * Fetch Meesho Login Credentials By Logging into meesho using puppeteer
 *
 * @since 1.0.0
 */
export declare function fetchMeeshoLoginCredentials({
  props: {
    credentials: { username, password }
  }
}: {
  props: {
    credentials: {
      username: string
      password: string
    }
  }
}): Promise<{
  result: {
    cookies: string
    sellerId: number
    marketplaceId: string
  }
}>
export declare function fetchMeeshoOrders(
  selector: string,
  url: string,
  status: number
): ({
  props: { credentials, filter }
}: {
  props: {
    credentials: {
      marketplaceId: string
      sellerId: number
      cookies: string
    }
    filter?: OrderFilters
  }
}) => any
export declare function fetchMeeshoReturns(
  selector: string,
  size?: number,
  sortOrder?: SortOrder
): ({
  props: { credentials, filter }
}: {
  props: {
    credentials: {
      marketplaceId: string
      sellerId: number
      cookies: string
    }
    filter?: ReturnFilters
  }
}) => any
export declare function fetchMeeshoPenalties(
  selector: string,
  size?: string,
  offset?: string
): ({
  props: { credentials }
}: {
  props: {
    credentials: {
      marketplaceId: string
      cookies: string
    }
  }
}) => any
export declare function fetchMeeshoInvoices(
  selector: string
): ({
  props: { credentials }
}: {
  props: {
    credentials: {
      marketplaceId: string
      cookies: string
    }
  }
}) => any
export declare function fetchMeeshoOutstandingPreviousPayments(
  selector: string,
  status: string,
  action: string
): ({
  props: { credentials }
}: {
  props: {
    credentials: {
      marketplaceId: string
      cookies: string
    }
  }
}) => any
export declare function fetchMeeshoPaymentDetails(
  selector: string,
  typeCall: string,
  date: string,
  status: string
): ({
  props: { credentials }
}: {
  props: {
    credentials: {
      marketplaceId: string
      sellerId: string
      cookies: string
    }
  }
}) => any
export declare function fetchShippedOrdersFileUrl(
  selector: string,
  min: string,
  max: string
): ({
  props: { credentials }
}: {
  props: {
    credentials: {
      cookies: string
      marketplaceId: string
    }
  }
}) => any
