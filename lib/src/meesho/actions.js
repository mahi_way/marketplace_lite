"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var puppeteer = require("puppeteer");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var helpers_1 = require("./helpers");
// import { meesho } from '../../credentials'
/**
 * Fetch Meesho Login Credentials By Logging into meesho using puppeteer
 *
 * @since 1.0.0
 */
function fetchMeeshoLoginCredentials(_a) {
    var _b = _a.props.credentials, _c = _b.username, username = _c === void 0 ? '' : _c, _d = _b.password, password = _d === void 0 ? '' : _d;
    return __awaiter(this, void 0, void 0, function () {
        var browser, page;
        var _this = this;
        return __generator(this, function (_e) {
            switch (_e.label) {
                case 0: return [4 /*yield*/, puppeteer.launch({ headless: true })];
                case 1:
                    browser = _e.sent();
                    return [4 /*yield*/, browser.newPage()];
                case 2:
                    page = _e.sent();
                    return [2 /*return*/, page
                            .goto('https://supplier.meeshosupply.com/login')
                            .then(function () { return page.waitForSelector('.login-input'); })
                            .then(function () { return page.$('.login-input'); })
                            .then(function (userInput) { return userInput.type(username); })
                            .then(function () { return page.$('.login-password'); })
                            .then(function (passwordInput) { return passwordInput.type(password); })
                            .then(function () { return page.click('.login-button'); })
                            .then(function () {
                            var request$ = rxjs_1.fromEvent(page, 'request').pipe(operators_1.map(function (request) { return request.postData(); }), operators_1.filter(function (val) { return !!val; }), operators_1.map(function (val) {
                                try {
                                    return JSON.parse(val);
                                }
                                catch (err) {
                                    return null;
                                }
                            }), operators_1.filter(function (val) { return !!val; }));
                            return rxjs_1.race(rxjs_1.timer(10000).pipe(operators_1.flatMap(function () { return rxjs_1.throwError('Could not fetch sellerId and marketplaceId'); })), rxjs_1.zip(request$.pipe(operators_1.filter(function (val) { return !!val.identifier; }), operators_1.map(function (_a) {
                                var identifier = _a.identifier;
                                return identifier;
                            }), operators_1.take(1)), request$.pipe(operators_1.filter(function (val) { return !!val.supplier_id; }), operators_1.map(function (_a) {
                                var supplier_id = _a.supplier_id;
                                return supplier_id;
                            }), operators_1.take(1)))).toPromise();
                        })
                            .then(function (val) { return __awaiter(_this, void 0, void 0, function () {
                            var cookie;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, page.goto("https://supplier.meeshosupply.com/" + val[1] + "/orders")];
                                    case 1:
                                        _a.sent();
                                        return [4 /*yield*/, page.cookies()];
                                    case 2:
                                        cookie = _a.sent();
                                        return [4 /*yield*/, browser.close()];
                                    case 3:
                                        _a.sent();
                                        return [2 /*return*/, {
                                                result: {
                                                    marketplaceId: val[0],
                                                    sellerId: val[1],
                                                    cookies: cookie.map(function (c) { return c.name + "=" + c.value; }).join('; ')
                                                }
                                            }];
                                }
                            });
                        }); })];
            }
        });
    });
}
exports.fetchMeeshoLoginCredentials = fetchMeeshoLoginCredentials;
function fetchMeeshoOrders(selector, url, status) {
    return function fetchMeeshoOrders$(_a) {
        var _b;
        var _c = _a.props, credentials = _c.credentials, filter = _c.filter;
        return _b = {},
            _b[selector] = helpers_1.fetchOrders({
                url: url,
                cookie: credentials.cookies,
                identifier: credentials.marketplaceId,
                supplierId: credentials.sellerId,
                status: status,
                filter: filter
            }),
            _b;
    };
}
exports.fetchMeeshoOrders = fetchMeeshoOrders;
function fetchMeeshoReturns(selector, size, sortOrder) {
    return function fetchMeeshoReturns$(_a) {
        var _b;
        var _c = _a.props, credentials = _c.credentials, filter = _c.filter;
        return _b = {},
            _b[selector] = helpers_1.fetchReturns({
                cookie: credentials.cookies,
                identifier: credentials.marketplaceId,
                supplierId: credentials.sellerId,
                filter: filter,
                size: size,
                sortOrder: sortOrder
            }).pipe(operators_1.expand(function (response) {
                return response.paging.next
                    ? helpers_1.fetchReturns({
                        cookie: credentials.cookies,
                        identifier: credentials.marketplaceId,
                        supplierId: credentials.sellerId,
                        pagingToken: response.paging.next
                    })
                    : rxjs_1.empty();
            })),
            _b;
    };
}
exports.fetchMeeshoReturns = fetchMeeshoReturns;
function fetchMeeshoPenalties(selector, size, offset) {
    return function fetchMeeshoPenalties$(_a) {
        var _b;
        var credentials = _a.props.credentials;
        return _b = {},
            _b[selector] = helpers_1.fetchPenalties({
                cookie: credentials.cookies,
                identifier: credentials.marketplaceId,
                size: size,
                offset: offset
            }),
            _b;
    };
}
exports.fetchMeeshoPenalties = fetchMeeshoPenalties;
function fetchMeeshoInvoices(selector) {
    return function fetchMeeshoInvoices$(_a) {
        var _b;
        var credentials = _a.props.credentials;
        return _b = {},
            _b[selector] = helpers_1.fetchInvoices({
                cookie: credentials.cookies,
                identifier: credentials.marketplaceId
            }),
            _b;
    };
}
exports.fetchMeeshoInvoices = fetchMeeshoInvoices;
function fetchMeeshoOutstandingPreviousPayments(selector, status, action) {
    return function fetchMeeshoOutstandingPreviousPayments$(_a) {
        var _b;
        var credentials = _a.props.credentials;
        return _b = {},
            _b[selector] = helpers_1.fetchOutstandingPreviousPayments({
                cookie: credentials.cookies,
                identifier: credentials.marketplaceId,
                status: status,
                action: action
            }),
            _b;
    };
}
exports.fetchMeeshoOutstandingPreviousPayments = fetchMeeshoOutstandingPreviousPayments;
function fetchMeeshoPaymentDetails(selector, typeCall, date, status) {
    return function fetchMeeshoPaymentDetails$(_a) {
        var _b;
        var credentials = _a.props.credentials;
        return _b = {},
            _b[selector] = helpers_1.fetchPayments({
                cookie: credentials.cookies,
                identifier: credentials.marketplaceId,
                supplierId: credentials.sellerId,
                typeCall: typeCall,
                date: date,
                status: status
            }),
            _b;
    };
}
exports.fetchMeeshoPaymentDetails = fetchMeeshoPaymentDetails;
function fetchShippedOrdersFileUrl(selector, min, max) {
    return function fetchShippedOrdersFileUrl$(_a) {
        var _b;
        var credentials = _a.props.credentials;
        return _b = {},
            _b[selector] = helpers_1.fetchOrderShippedUrl({
                cookie: credentials.cookies,
                identifier: credentials.marketplaceId,
                min: min,
                max: max
            }),
            _b;
    };
}
exports.fetchShippedOrdersFileUrl = fetchShippedOrdersFileUrl;
// const { pendingOrder$ } = fetchMeeshoOrders('pendingOrder$', 'https://supplier.meeshosupply.com/api/v2/order/fetch/all', 6)({ props: { credentials: meesho.violina } })
// pendingOrder$.subscribe(val => {
//   console.log(JSON.stringify(val))
// })
