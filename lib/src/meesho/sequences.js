"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var function_tree_1 = require("function-tree");
var actions_1 = require("./actions");
exports.fetchMeeshoOrdersSequence = function_tree_1.sequence('Fetch Meesho Orders', [
    actions_1.fetchMeeshoOrders('pendingOrder$', 'https://supplier.meeshosupply.com/api/v1/order/fetch/all', 0),
    actions_1.fetchMeeshoOrders('dispacthableOrder$', 'https://supplier.meeshosupply.com/api/v1/order/fetch/all', 0),
    actions_1.fetchMeeshoOrders('readyToShipOrder$', 'https://supplier.meeshosupply.com/api/v1/order/fetch/all', 0),
    function mergeObs(_a) {
        var props = _a.props;
        console.log(props);
    }
]);
