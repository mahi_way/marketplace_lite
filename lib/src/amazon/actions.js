"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ramda_1 = require("ramda");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var parser = require("fast-xml-parser");
var nodejs_1 = require("./nodejs");
var retry = require('retry');
// Helpers
var retryStrategyShort = {
    retries: 6,
    factor: 3,
    minTimeout: 2 * 1000
};
var retryStrategyMedium = {
    retries: 8,
    factor: 4,
    minTimeout: 2 * 1000
};
var retryStrategyLong = {
    retries: 10,
    factor: 5,
    minTimeout: 2 * 1000
};
/**
 * @param authfetch - MWSClient
 * @returns { valid, error }
 */
exports.checkOrderServiceStatus = function (_a) {
    var authfetch = _a.props.authfetch;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_b) {
            return [2 /*return*/, new Promise(function (resolve, reject) {
                    authfetch.ListMarketplaceParticipations(function (err, res) {
                        if (err) {
                            // console.log(err);
                            var error = parser.parse(err.body);
                            // console.log('Error -', error);
                            reject(new Error(error.ErrorResponse.Error.Message));
                        }
                        else {
                            if (res.status === 200) {
                                resolve({ valid: true });
                            }
                            else {
                                reject({ valid: false, error: res });
                            }
                        }
                    });
                })
                /**
                 * @param credentials - { marketplace, accesskey, secret, sellerID, mwsAuthToken }
                 */
            ];
        });
    });
};
/**
 * @param credentials - { marketplace, accesskey, secret, sellerID, mwsAuthToken }
 */
exports.createAmazonAuthfetch = function (_a) {
    var credentials = _a.props.credentials;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_b) {
            return [2 /*return*/, ({
                    authfetch: new nodejs_1.NodeJSMWSClient(ramda_1.compose(ramda_1.tail, ramda_1.split(' '))(credentials.marketplace), credentials.appId, credentials.appSecret, credentials.sellerId, credentials.authToken)
                })
                /*
                 * ----
                 * Observer Functions
                 * ----
                 */
            ];
        });
    });
};
/*
 * ----
 * Observer Functions
 * ----
 */
var downloadReport$ = function (authfetch) { return function (reportId) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        if (reportId === null) {
            resolve('');
        }
        else {
            var operation_1 = retry.operation(retryStrategyLong);
            operation_1.attempt(function () {
                authfetch.GetReport({ ReportId: reportId }, function (error, response) {
                    if (error) {
                        if (operation_1.retry(error)) {
                            return;
                        }
                        reject(error);
                    }
                    else {
                        // console.log('GOT DOWNLOAD RESPONSE');
                        resolve(response.body);
                    }
                });
            });
        }
    }));
}; };
var orderItems$ = function (authfetch) { return function (orderId) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        var operation = retry.operation(retryStrategyShort);
        operation.attempt(function () {
            authfetch.ListOrderItems(orderId, function (error, response) {
                // console.log(orderId);
                if (error) {
                    // console.log(error);
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    // console.log('resolved');
                    resolve((parser.parse(response.body)).ListOrderItemsResponse.ListOrderItemsResult);
                }
            });
        });
    }));
}; };
var orderListNext$ = function (authfetch) { return function (NextToken) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        var operation = retry.operation(retryStrategyShort);
        operation.attempt(function () {
            authfetch.ListOrdersByNextToken({
                NextToken: NextToken
            }, function (error, response) {
                if (error) {
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    resolve((parser.parse(response.body)).ListOrdersByNextTokenResponse.ListOrdersByNextTokenResult);
                }
            });
        });
    }));
}; };
var competitivePricing$ = function (authfetch) { return function (args) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        var operation = retry.operation(retryStrategyShort);
        operation.attempt(function () {
            authfetch.GetCompetitivePricingForASIN(args, function (error, response) {
                // console.log(orderId);
                if (error) {
                    // console.log(error);
                    if (error.status === 400) {
                        reject(error);
                    }
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    // console.log('resolved');
                    resolve((parser.parse(response.body)));
                }
            });
        });
    }));
}; };
var feeEstimate$ = function (authfetch) { return function (args) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        var operation = retry.operation(retryStrategyShort);
        operation.attempt(function () {
            authfetch.GetMyFeesEstimate(args, function (error, response) {
                // console.log(orderId);
                if (error) {
                    // console.log(error);
                    if (error.status === 400) {
                        reject(error);
                    }
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    // console.log('resolved');
                    resolve((parser.parse(response.body)));
                }
            });
        });
    }));
}; };
var lowestOfferListings$ = function (authfetch) { return function (args) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        var operation = retry.operation(retryStrategyShort);
        operation.attempt(function () {
            authfetch.GetLowestOfferListingsForASIN(args, function (error, response) {
                if (error) {
                    if (error.status === 400) {
                        reject(error);
                    }
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    // console.log('resolved');
                    resolve((parser.parse(response.body)));
                }
            });
        });
    }));
}; };
// Handle error for 'Failed processing arguments of org.jboss.resteasy.spi.metadata.ResourceMethod'
// ref - https://stackoverflow.com/questions/36293752/getlowestpricedoffersforsku-failed-processing-arguments
var lowestPricedOffers$ = function (authfetch) { return function (args) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        var operation = retry.operation(retryStrategyShort);
        operation.attempt(function () {
            authfetch.GetLowestPricedOffersForASIN(args, function (error, response) {
                if (error) {
                    if (error.status === 400) {
                        reject(error);
                    }
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    // console.log('resolved');
                    resolve((parser.parse(response.body)));
                }
            });
        });
    }));
}; };
var matchingProduct$ = function (authfetch) { return function (args) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        var operation = retry.operation(retryStrategyShort);
        operation.attempt(function () {
            authfetch.GetMatchingProduct(args, function (error, response) {
                // console.log(orderId);
                if (error) {
                    // console.log(error);
                    if (error.status === 400) {
                        reject(error);
                    }
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    // console.log('resolved');
                    resolve((parser.parse(response.body)));
                }
            });
        });
    }));
}; };
var productCategories$ = function (authfetch) { return function (args) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        var operation = retry.operation(retryStrategyShort);
        operation.attempt(function () {
            authfetch.GetProductCategoriesForASIN(args, function (error, response) {
                // console.log(orderId);
                if (error) {
                    // console.log(error);
                    if (error.status === 400) {
                        reject(error);
                    }
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    // console.log('resolved');
                    resolve((parser.parse(response.body)));
                }
            });
        });
    }));
}; };
var reportListNext$ = function (authfetch) { return function (NextToken) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        // console.log('REQUESTION REPORT');
        var operation = retry.operation(retryStrategyMedium);
        operation.attempt(function () {
            authfetch.GetReportListByNextToken({
                NextToken: NextToken
            }, function (error, response) {
                if (error) {
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    resolve((parser.parse(response.body)).GetReportListByNextTokenResponse.GetReportListByNextTokenResult);
                }
            });
        });
    }));
}; };
var reportResult$ = function (authfetch) { return function (reportId) {
    return rxjs_1.from(new Promise(function (resolve, reject) {
        var operation = retry.operation(retryStrategyMedium);
        operation.attempt(function () {
            authfetch.GetReportRequestList({ 'ReportRequestIdList.Id.1': reportId }, function (error, res) {
                if (error) {
                    if (operation.retry(error)) {
                        return;
                    }
                    reject(error);
                }
                else {
                    var response = parser.parse(res.body, { parseTrueNumberOnly: true });
                    // console.log(JSON.stringify(response))
                    if (response.GetReportRequestListResponse.GetReportRequestListResult.ReportRequestInfo
                        .ReportProcessingStatus === '_DONE_') {
                        resolve(response.GetReportRequestListResponse.GetReportRequestListResult.ReportRequestInfo.GeneratedReportId);
                    }
                    else if (response.GetReportRequestListResponse.GetReportRequestListResult.ReportRequestInfo
                        .ReportProcessingStatus === '_DONE_NO_DATA_') {
                        resolve(null);
                    }
                    else if (response.GetReportRequestListResponse.GetReportRequestListResult.ReportRequestInfo
                        .ReportProcessingStatus === '_CANCELLED_') {
                        // console.log('AWESOME CANCELLED DATAAA');
                        // console.log(response.GetReportRequestListResponse);
                        // resolve(null);
                        reject({
                            status: 404,
                            message: 'CANCELLED'
                        });
                    }
                    else {
                        if (operation.retry({
                            status: 555,
                            message: response
                        })) {
                            return;
                        }
                        reject({
                            status: 555,
                            message: response
                        });
                    }
                }
            });
        });
    }));
}; };
/*
 * ----
 * Observer Actions
 * ----
 */
exports.createAmazonOrderIdsBatch$ = function (_a) {
    var orderIds$ = _a.props.orderIds$;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_b) {
            return [2 /*return*/, ({
                    orderIdsBatch$: orderIds$.pipe(operators_1.bufferCount(50)
                    // tap(val => {
                    //   console.log('CREATED ARRAY');
                    //   console.log(val);
                    // })
                    )
                })];
        });
    });
};
exports.fetchAmazonMyFeeEstimate$ = function (type) {
    if (type === void 0) { type = 'marketplace'; }
    return function fetchAmazonMyFeeEstimate$(_a) {
        var _b = _a.props, authfetch = _b.authfetch, credentials = _b.credentials, asin = _b.asin, amount = _b.amount, _c = _b.currency, currency = _c === void 0 ? 'INR' : _c;
        return __awaiter(this, void 0, void 0, function () {
            var _d;
            return __generator(this, function (_e) {
                return [2 /*return*/, (_d = {},
                        _d[type + "FeeEstimate$"] = feeEstimate$(authfetch)({
                            'FeesEstimateRequestList.FeesEstimateRequest.1.MarketplaceId': credentials.marketplaceId,
                            'FeesEstimateRequestList.FeesEstimateRequest.1.IdType': 'ASIN',
                            'FeesEstimateRequestList.FeesEstimateRequest.1.IdValue': asin,
                            'FeesEstimateRequestList.FeesEstimateRequest.1.IsAmazonFulfilled': type.toLowerCase() === 'marketplace',
                            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.ListingPrice.Amount': amount,
                            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.ListingPrice.CurrencyCode': currency,
                            'FeesEstimateRequestList.FeesEstimateRequest.1.Identifier': new Date().toISOString()
                        }).pipe(operators_1.map(function (_a) {
                            var FeesEstimate = _a.GetMyFeesEstimateResponse.GetMyFeesEstimateResult.FeesEstimateResultList.FeesEstimateResult.FeesEstimate;
                            return FeesEstimate;
                        })),
                        _d)];
            });
        });
    };
};
exports.fetchAmazonCompetitivePricing$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, credentials = _b.credentials, asin = _b.asin;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    competitivePricing$: competitivePricing$(authfetch)({
                        'MarketplaceId': credentials.marketplaceId,
                        'ASINList.ASIN.1': asin
                    })
                })];
        });
    });
};
exports.fetchAmazonLowestOfferListings$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, credentials = _b.credentials, asin = _b.asin;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    lowestOfferListings$: lowestOfferListings$(authfetch)({
                        'MarketplaceId': credentials.marketplaceId,
                        'ASINList.ASIN.1': asin,
                        'ItemCondition': 'New',
                        'ExcludeMe': true
                    }).pipe(operators_1.map(function (_a) {
                        var Product = _a.GetLowestOfferListingsForASINResponse.GetLowestOfferListingsForASINResult.Product;
                        return Product;
                    }))
                })];
        });
    });
};
exports.fetchAmazonGetLowestPricedOffers$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, credentials = _b.credentials, asin = _b.asin;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    lowestPricedOffers$: lowestPricedOffers$(authfetch)({
                        'MarketplaceId': credentials.marketplaceId,
                        'ASIN': asin,
                        'ItemCondition': 'New'
                    }).pipe(operators_1.map(function (_a) {
                        var GetLowestPricedOffersForASINResult = _a.GetLowestPricedOffersForASINResponse.GetLowestPricedOffersForASINResult;
                        return GetLowestPricedOffersForASINResult;
                    }))
                })];
        });
    });
};
exports.fetchAmazonMatchingProduct$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, credentials = _b.credentials, asin = _b.asin;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    matchingProduct$: matchingProduct$(authfetch)({
                        'MarketplaceId': credentials.marketplaceId,
                        'ASINList.ASIN.1': asin
                    })
                })];
        });
    });
};
exports.fetchAmazonProductCategories$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, credentials = _b.credentials, asin = _b.asin;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    productCategories$: productCategories$(authfetch)({
                        'MarketplaceId': credentials.marketplaceId,
                        'ASIN': asin
                    }).pipe(operators_1.map(function (_a) {
                        var Self = _a.GetProductCategoriesForASINResponse.GetProductCategoriesForASINResult.Self;
                        return Self;
                    }))
                })];
        });
    });
};
exports.fetchOrderItems$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, orderListNext$ = _b.orderListNext$;
    return ({
        orderItems$: orderListNext$.pipe(operators_1.delay(900), operators_1.concatMap(function (_a) {
            var AmazonOrderId = _a.AmazonOrderId;
            return orderItems$(authfetch)({ AmazonOrderId: AmazonOrderId });
        }, function (order, _a) {
            var OrderItems = _a.OrderItems;
            if (Array.isArray(OrderItems.OrderItem)) {
                return OrderItems.OrderItem.map(function (item) { return (__assign(__assign({}, order), { item: item })); });
            }
            else {
                return [__assign(__assign({}, order), { item: OrderItems.OrderItem, orderItemId: OrderItems.OrderItem.OrderItemId })];
            }
        })
        // tap(res => {
        //   console.log('GOT RESULT');
        //   console.log(res);
        // })
        )
    });
};
exports.fetchOrderList$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, fetchOrderListParams = _b.fetchOrderListParams;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, ({
                    orderList$: rxjs_1.from(new Promise(function (resolve, reject) {
                        var operation = retry.operation(retryStrategyShort);
                        operation.attempt(function () {
                            authfetch.ListOrders(fetchOrderListParams, function (error, response) {
                                if (error) {
                                    if (operation.retry(error)) {
                                        return;
                                    }
                                    reject(error);
                                }
                                else {
                                    resolve((parser.parse(response.body)).ListOrdersResponse.ListOrdersResult);
                                }
                            });
                        });
                    }))
                })];
        });
    });
};
exports.fetchOrderListNext$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, orderList$ = _b.orderList$;
    return ({
        orderListNext$: orderList$.pipe(
        // tap(console.log),
        // tap(() => { console.log('*****') }),
        operators_1.expand(function (_a) {
            var NextToken = _a.NextToken;
            return NextToken ? orderListNext$(authfetch)(NextToken).pipe(operators_1.delay(10000)) : rxjs_1.empty();
        }), operators_1.concatMap(function (_a) {
            var Orders = _a.Orders;
            return Orders ? Array.isArray(Orders.Order) ? Orders.Order : [Orders.Order] : rxjs_1.empty();
        })
        // tap(console.log)
        // toArray()
        )
    });
};
exports.fetchOrderIdsBatch$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, orderIdsBatch$ = _b.orderIdsBatch$;
    return ({
        orderListNext$: orderIdsBatch$.pipe(operators_1.concatMap(function (orderIdsBatch) { return rxjs_1.from(new Promise(function (resolve, reject) {
            var operation = retry.operation(retryStrategyShort);
            var rqstIds = orderIdsBatch.reduce(function (acc, curr, index) {
                var _a;
                return (__assign(__assign({}, acc), (_a = {}, _a["AmazonOrderId.Id." + (index + 1)] = curr, _a)));
            }, {});
            operation.attempt(function () {
                authfetch.GetOrder(rqstIds, function (error, response) {
                    if (error) {
                        if (operation.retry(error)) {
                            return;
                        }
                        reject(error);
                    }
                    else {
                        resolve((parser.parse(response.body)).GetOrderResponse.GetOrderResult);
                    }
                });
            });
        })); }), 
        // tap(val => {
        //   console.log('Parsed Values');
        //   console.log(val);
        //   // console.log(typeof val.Orders.Order);
        // }),
        operators_1.concatMap(function (_a) {
            var Orders = _a.Orders;
            if (Orders) {
                return Array.isArray(Orders.Order) ? Orders.Order : [Orders.Order];
            }
            return rxjs_1.empty();
        }))
    });
};
exports.getReport$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, generatedReportId$ = _b.generatedReportId$;
    return ({
        report$: generatedReportId$.pipe(operators_1.concatMap(downloadReport$(authfetch)))
    });
};
exports.getReportById$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, reportId = _b.reportId;
    return ({
        report$: downloadReport$(authfetch)(reportId)
    });
};
exports.requestReport$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, requestReportParams = _b.requestReportParams;
    return ({
        requestedReportId$: rxjs_1.from(new Promise(function (resolve, reject) {
            var operation = retry.operation(retryStrategyShort);
            operation.attempt(function () {
                authfetch.RequestReport(requestReportParams, function (error, response) {
                    if (error) {
                        if (operation.retry(error)) {
                            return;
                        }
                        reject(error);
                    }
                    else {
                        resolve((parser.parse(response.body, { parseTrueNumberOnly: true })).RequestReportResponse.RequestReportResult.ReportRequestInfo.ReportRequestId);
                    }
                });
            });
        }))
    });
};
exports.requestReportList$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, requestReportParams = _b.requestReportParams;
    return ({
        reportList$: rxjs_1.from(new Promise(function (resolve, reject) {
            var operation = retry.operation(retryStrategyShort);
            operation.attempt(function () {
                authfetch.GetReportList(requestReportParams, function (error, response) {
                    if (error) {
                        if (operation.retry(error)) {
                            return;
                        }
                        reject(error);
                    }
                    else {
                        resolve((parser.parse(response.body, { parseTrueNumberOnly: true })).GetReportListResponse.GetReportListResult);
                    }
                });
            });
        }))
    });
};
exports.requestReportListNext$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, reportList$ = _b.reportList$;
    return ({
        reportListNext$: reportList$.pipe(
        // tap(console.log),
        operators_1.expand(function (_a) {
            var NextToken = _a.NextToken;
            return NextToken ? reportListNext$(authfetch)(NextToken).pipe(operators_1.delay(10000)) : rxjs_1.empty();
        }), operators_1.concatMap(function (_a) {
            var ReportInfo = _a.ReportInfo;
            return ReportInfo ? Array.isArray(ReportInfo) ? ReportInfo : [ReportInfo] : rxjs_1.empty();
        }))
    });
};
exports.requestReportResult$ = function (_a) {
    var _b = _a.props, authfetch = _b.authfetch, requestedReportId$ = _b.requestedReportId$;
    return ({
        generatedReportId$: requestedReportId$.pipe(operators_1.delay(30000), operators_1.concatMap(reportResult$(authfetch)))
    });
};
exports.tsv2json$ = function (_a) {
    var _b = _a.props, report$ = _b.report$, _c = _b.tsvSeperator, tsvSeperator = _c === void 0 ? '\r\n' : _c;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_d) {
            return [2 /*return*/, ({
                    json$: report$.pipe(operators_1.concatMap(function (tsv) {
                        // console.log(tsv);
                        var arr = tsv.split(tsvSeperator);
                        var header = arr[0].split('\t');
                        var rows$ = rxjs_1.from(arr).pipe(operators_1.skip(1), operators_1.map(function (row) { return row.split('\t'); }));
                        return rows$.pipe(operators_1.map(function (row) {
                            return row.reduce(function (rowObj, cell, i) {
                                // @ts-ignore
                                rowObj[header[i]] = cell;
                                return rowObj;
                            }, {});
                        }));
                    }))
                })];
        });
    });
};
exports.xml2json$ = function (_a) {
    var report$ = _a.props.report$;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_b) {
            return [2 /*return*/, ({
                    json$: report$.pipe(operators_1.map(function (val) { return parser.parse(val); }))
                })
                /*
                 * ----
                 * Subscriptions
                 * ----
                 */
                // export const subscribeJson = async ({ props: { json$ } }) =>
                // new Promise((resolve) => {
                //   json$.pipe(toArray()).subscribe(json => {
                //     resolve({ json })
                //   })
                // })
                // export const subscribeOrderItems = ({ props: { orderItems$ } }) =>
                // new Promise((resolve, reject) => {
                //   let orderItems = []
                //   orderItems$
                //     .pipe(
                //       toArray(),
                //       map(arr => flatten(arr)),
                //       catchError(err => Promise.reject(err))
                //     )
                //     .subscribe({
                //       next: result => {
                //         // console.log('NEXT CALLED');
                //         orderItems = result
                //       },
                //       complete: () => {
                //         // console.log('COMPLETED');
                //         resolve({ orderItems })
                //       },
                //       error: err => {
                //         // console.log('CAUGHT ERRROR');
                //         // console.log(err);
                //         reject(new Error(JSON.stringify(err)))
                //       }
                //     })
                // })
                // export const subscribeReport = ({ props: { report$ } }) =>
                // new Promise((resolve, reject) => {
                //   report$.pipe(catchError(err => Promise.reject(err))).subscribe(response => resolve({ response }), err => reject(new Error(JSON.stringify(err))))
                // })
                // export const subscribeReportList = ({ props: { reportListNext$ } }) =>
                // new Promise((resolve, reject) => {
                //   reportListNext$.pipe(toArray(), catchError(err => Promise.reject(err))).subscribe(response => resolve({ response }), err => reject(new Error(JSON.stringify(err))))
                // })
                /*
                * -- Combinator Actions
                */
            ];
        });
    });
};
/*
 * ----
 * Subscriptions
 * ----
 */
// export const subscribeJson = async ({ props: { json$ } }) =>
// new Promise((resolve) => {
//   json$.pipe(toArray()).subscribe(json => {
//     resolve({ json })
//   })
// })
// export const subscribeOrderItems = ({ props: { orderItems$ } }) =>
// new Promise((resolve, reject) => {
//   let orderItems = []
//   orderItems$
//     .pipe(
//       toArray(),
//       map(arr => flatten(arr)),
//       catchError(err => Promise.reject(err))
//     )
//     .subscribe({
//       next: result => {
//         // console.log('NEXT CALLED');
//         orderItems = result
//       },
//       complete: () => {
//         // console.log('COMPLETED');
//         resolve({ orderItems })
//       },
//       error: err => {
//         // console.log('CAUGHT ERRROR');
//         // console.log(err);
//         reject(new Error(JSON.stringify(err)))
//       }
//     })
// })
// export const subscribeReport = ({ props: { report$ } }) =>
// new Promise((resolve, reject) => {
//   report$.pipe(catchError(err => Promise.reject(err))).subscribe(response => resolve({ response }), err => reject(new Error(JSON.stringify(err))))
// })
// export const subscribeReportList = ({ props: { reportListNext$ } }) =>
// new Promise((resolve, reject) => {
//   reportListNext$.pipe(toArray(), catchError(err => Promise.reject(err))).subscribe(response => resolve({ response }), err => reject(new Error(JSON.stringify(err))))
// })
/*
* -- Combinator Actions
*/
exports.combineAsinProductInfo$ = function (_a) {
    var _b = _a.props, marketplaceFeeEstimate$ = _b.marketplaceFeeEstimate$, merchantFeeEstimate$ = _b.merchantFeeEstimate$, lowestPricedOffers$ = _b.lowestPricedOffers$, lowestOfferListings$ = _b.lowestOfferListings$, productCategories$ = _b.productCategories$;
    return ({
        productInfo$: rxjs_1.forkJoin({
            marketplaceEstimate: marketplaceFeeEstimate$,
            merchantFeeEstimate: merchantFeeEstimate$,
            summary: lowestPricedOffers$,
            offers: lowestOfferListings$,
            category: productCategories$
        })
    });
};
/*
* -- Test Helpers
*/
exports.orderIdsObservable = function (_a) {
    var orderIds = _a.props.orderIds;
    return ({
        orderIds$: rxjs_1.from(orderIds)
    });
};
