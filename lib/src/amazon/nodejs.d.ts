import { MWSClientBase } from './base'
export declare class NodeJSMWSClient extends MWSClientBase {
  calcMD5(content: any): string
  calcHMAC(content: any, secret: any): string
  makeHttpRequest(method: any, url: any, headers: any, body: any, cbk: any): void
  makeHttpRequestFormData(url: any, headers: any, form: any, cbk: any): void
  encodeContent(content: string, encoding: string): any
  getUserAgent(): string
}
