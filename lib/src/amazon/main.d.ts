declare const _default: {
  US: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  CA: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  MX: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  DE: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  ES: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  FR: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  IT: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  UK: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  IN: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  AU: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  JP: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  CN: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
  BR: {
    name: string
    site: string
    id: string
    mws_endpoint: string
    region: string
  }
}
export default _default
