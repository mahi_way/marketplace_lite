import { NodeJSMWSClient as MWSClient } from './nodejs'
/**
 * @param authfetch - MWSClient
 * @returns { valid, error }
 */
export declare const checkOrderServiceStatus: ({
  props: { authfetch }
}: {
  props: {
    authfetch: any
  }
}) => Promise<unknown>
/**
 * @param credentials - { marketplace, accesskey, secret, sellerID, mwsAuthToken }
 */
export declare const createAmazonAuthfetch: ({
  props: { credentials }
}: {
  props: {
    credentials: any
  }
}) => Promise<{
  authfetch: MWSClient
}>
export declare const createAmazonOrderIdsBatch$: ({
  props: { orderIds$ }
}: {
  props: {
    orderIds$: any
  }
}) => Promise<{
  orderIdsBatch$: any
}>
export declare const fetchAmazonMyFeeEstimate$: (
  type?: string
) => ({
  props: { authfetch, credentials, asin, amount, currency }
}: {
  props: {
    authfetch: any
    credentials: any
    asin: any
    amount: any
    currency?: string
  }
}) => Promise<{
  [x: string]: import('rxjs').Observable<any>
}>
export declare const fetchAmazonCompetitivePricing$: ({
  props: { authfetch, credentials, asin }
}: {
  props: {
    authfetch: any
    credentials: any
    asin: any
  }
}) => Promise<{
  competitivePricing$: import('rxjs').Observable<unknown>
}>
export declare const fetchAmazonLowestOfferListings$: ({
  props: { authfetch, credentials, asin }
}: {
  props: {
    authfetch: any
    credentials: any
    asin: any
  }
}) => Promise<{
  lowestOfferListings$: import('rxjs').Observable<any>
}>
export declare const fetchAmazonGetLowestPricedOffers$: ({
  props: { authfetch, credentials, asin }
}: {
  props: {
    authfetch: any
    credentials: any
    asin: any
  }
}) => Promise<{
  lowestPricedOffers$: import('rxjs').Observable<any>
}>
export declare const fetchAmazonMatchingProduct$: ({
  props: { authfetch, credentials, asin }
}: {
  props: {
    authfetch: any
    credentials: any
    asin: any
  }
}) => Promise<{
  matchingProduct$: import('rxjs').Observable<unknown>
}>
export declare const fetchAmazonProductCategories$: ({
  props: { authfetch, credentials, asin }
}: {
  props: {
    authfetch: any
    credentials: any
    asin: any
  }
}) => Promise<{
  productCategories$: import('rxjs').Observable<any>
}>
export declare const fetchOrderItems$: ({
  props: { authfetch, orderListNext$ }
}: {
  props: {
    authfetch: any
    orderListNext$: any
  }
}) => {
  orderItems$: any
}
export declare const fetchOrderList$: ({
  props: { authfetch, fetchOrderListParams }
}: {
  props: {
    authfetch: any
    fetchOrderListParams: any
  }
}) => Promise<{
  orderList$: import('rxjs').Observable<unknown>
}>
export declare const fetchOrderListNext$: ({
  props: { authfetch, orderList$ }
}: {
  props: {
    authfetch: any
    orderList$: any
  }
}) => {
  orderListNext$: any
}
export declare const fetchOrderIdsBatch$: ({
  props: { authfetch, orderIdsBatch$ }
}: {
  props: {
    authfetch: any
    orderIdsBatch$: any
  }
}) => {
  orderListNext$: any
}
export declare const getReport$: ({
  props: { authfetch, generatedReportId$ }
}: {
  props: {
    authfetch: any
    generatedReportId$: any
  }
}) => {
  report$: any
}
export declare const getReportById$: ({
  props: { authfetch, reportId }
}: {
  props: {
    authfetch: any
    reportId: any
  }
}) => {
  report$: import('rxjs').Observable<unknown>
}
export declare const requestReport$: ({
  props: { authfetch, requestReportParams }
}: {
  props: {
    authfetch: any
    requestReportParams: any
  }
}) => {
  requestedReportId$: import('rxjs').Observable<unknown>
}
export declare const requestReportList$: ({
  props: { authfetch, requestReportParams }
}: {
  props: {
    authfetch: any
    requestReportParams: any
  }
}) => {
  reportList$: import('rxjs').Observable<unknown>
}
export declare const requestReportListNext$: ({
  props: { authfetch, reportList$ }
}: {
  props: {
    authfetch: any
    reportList$: any
  }
}) => {
  reportListNext$: any
}
export declare const requestReportResult$: ({
  props: { authfetch, requestedReportId$ }
}: {
  props: {
    authfetch: any
    requestedReportId$: any
  }
}) => {
  generatedReportId$: any
}
export declare const tsv2json$: ({
  props: { report$, tsvSeperator }
}: {
  props: {
    report$: any
    tsvSeperator?: string
  }
}) => Promise<{
  json$: any
}>
export declare const xml2json$: ({
  props: { report$ }
}: {
  props: {
    report$: any
  }
}) => Promise<{
  json$: any
}>
export declare const combineAsinProductInfo$: ({
  props: {
    marketplaceFeeEstimate$,
    merchantFeeEstimate$,
    lowestPricedOffers$,
    lowestOfferListings$,
    productCategories$
  }
}: {
  props: {
    marketplaceFeeEstimate$: any
    merchantFeeEstimate$: any
    lowestPricedOffers$: any
    lowestOfferListings$: any
    productCategories$: any
  }
}) => {
  productInfo$: import('rxjs').Observable<{
    marketplaceEstimate: unknown
    merchantFeeEstimate: unknown
    summary: unknown
    offers: unknown
    category: unknown
  }>
}
export declare const orderIdsObservable: ({
  props: { orderIds }
}: {
  props: {
    orderIds: any
  }
}) => {
  orderIds$: import('rxjs').Observable<unknown>
}
