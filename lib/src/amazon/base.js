"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var xmljs = require('xml-js');
var mws_info = require('./main').default;
var api_mapping = {
    Feeds: {
        path: 'Feeds',
        version: '2009-01-01'
    },
    Finances: {
        path: 'Finances',
        version: '2015-05-01'
    },
    FulfillmentInboundShipment: {
        path: 'FulfillmentInboundShipment',
        version: '2010-10-01'
    },
    FulfillmentInventory: {
        path: 'FulfillmentInventory',
        version: '2010-10-01'
    },
    FulfillmentOutboundShipment: {
        path: 'FulfillmentOutboundShipment',
        version: '2010-10-01'
    },
    MerchantFulfillment: {
        path: 'MerchantFulfillment',
        version: '2015-06-01'
    },
    Orders: {
        path: 'Orders',
        version: '2013-09-01'
    },
    Products: {
        path: 'Products',
        version: '2011-10-01'
    },
    Recommendations: {
        path: 'Recommendations',
        version: '2013-04-01'
    },
    Reports: {
        path: 'Reports',
        version: '2009-01-01'
    },
    Sellers: {
        path: 'Sellers',
        version: '2011-07-01'
    },
    Subscriptions: {
        path: 'Subscriptions',
        version: '2013-07-01'
    },
    EasyShip: {
        path: 'EasyShip',
        version: '2018-09-01'
    }
};
var MWSClientBase = /** @class */ (function () {
    function MWSClientBase(marketplace, access_key, access_secret, seller_id, auth_token) {
        if (auth_token === void 0) { auth_token = ''; }
        var marketplace_info = this.getMarketplaceInfo(marketplace);
        this.useragent = null;
        this.marketplace = marketplace;
        this.marketplace_id = marketplace_info['id'];
        this.protocol = 'https';
        this.endpoint = marketplace_info['mws_endpoint'];
        this.site_url = marketplace_info['site'];
        this.access_key = access_key;
        this.access_secret = access_secret;
        this.seller_id = seller_id;
        this.auth_token = auth_token;
    }
    MWSClientBase.version = function () {
        return 'MWS_CLIENT_VERSION';
    };
    MWSClientBase.escapeUserAgentFieldValue = function (v) {
        /**
         * Escape special chars in user agent field value
         * @see http://docs.developer.amazonservices.com/en_US/dev_guide/DG_UserAgentHeader.html
         */
        return v.replace(/[\\\/();=]/g, function (m) {
            return '\\' + m;
        });
    };
    MWSClientBase.getMarketplaceId = function (country_code) {
        country_code = country_code.toUpperCase();
        if (!mws_info[country_code]) {
            throw new Error("Invalid country code: " + country_code);
        }
        return mws_info[country_code]['id'];
    };
    /**
     * Return null when response code is 20x, for others, return an error object with all returned data inside it
     */
    MWSClientBase.parseResponseError = function (status, headers, body) {
        status = parseInt(status, 10);
        if (status > 199 && status < 300) {
            // verify Content MD5
            if (headers['Content-MD5']) {
                throw new Error('Reponse content MD5 checksum not match!');
            }
            return null;
        }
        else {
            var err = {
                status: status,
                headers: headers,
                quota: {},
                body: body
            };
            if (headers['x-mws-quota-max']) {
                err['quota']['max'] = parseInt(headers['x-mws-quota-max'], 10);
            }
            if (headers['x-mws-quota-remaining']) {
                err['quota']['remaining'] = parseInt(headers['x-mws-quota-remaining'], 10);
            }
            if (headers['x-mws-quota-resetsOn'] || headers['x-mws-quota-resetson']) {
                err['quota']['resetsOn'] = new Date(headers['x-mws-quota-resetsOn'] || headers['x-mws-quota-resetson']);
            }
            if (headers['x-mws-request-id']) {
                err['request-id'] = headers['x-mws-request-id'];
            }
            if (headers['x-mws-response-context']) {
                err['response-context'] = headers['x-mws-response-context'];
            }
            if (headers['x-mws-timestamp']) {
                err['timestamp'] = new Date(headers['x-mws-timestamp']);
            }
            // detailed error message in response body
            if (headers['Content-Type'] == 'text/xml' || headers['content-type'] == 'text/xml') {
                // parse xml data
                err['data'] = xmljs.xml2js(body);
            }
            else {
                err['data'] = body;
            }
            return err;
        }
    };
    // only return a data object when response code is 20x, when not, just return null
    MWSClientBase.parseResponse = function (status, headers, body) {
        status = parseInt(status, 10);
        if (status > 199 && status < 300) {
            var res = {
                status: status,
                headers: headers,
                body: body,
                quota: {}
            };
            if (headers['x-mws-quota-max']) {
                res['quota']['max'] = parseInt(headers['x-mws-quota-max'], 10);
            }
            if (headers['x-mws-quota-remaining']) {
                res['quota']['remaining'] = parseInt(headers['x-mws-quota-remaining'], 10);
            }
            if (headers['x-mws-quota-resetsOn'] || headers['x-mws-quota-resetson']) {
                res['quota']['resetsOn'] = new Date(headers['x-mws-quota-resetsOn'] || headers['x-mws-quota-resetson']);
            }
            if (headers['x-mws-request-id']) {
                res['request-id'] = headers['x-mws-request-id'];
            }
            if (headers['x-mws-response-context']) {
                res['response-context'] = headers['x-mws-response-context'];
            }
            if (headers['x-mws-timestamp']) {
                res['timestamp'] = new Date(headers['x-mws-timestamp']);
            }
            if (headers['Content-Type'] == 'text/xml' || headers['content-type'] == 'text/xml') {
                // parse xml data
                res['data'] = xmljs.xml2js(body, { compact: true });
            }
            else {
                res['data'] = body;
            }
            return res;
        }
        else {
            return null;
        }
    };
    MWSClientBase.prototype.getMarketplaceInfo = function (country_code) {
        if (!mws_info[country_code]) {
            throw new Error('Invalid country code: ' + country_code);
        }
        return mws_info[country_code];
    };
    MWSClientBase.prototype.urlEncode = function (str) {
        return encodeURIComponent(str).replace(/[!*()']/g, function (c) {
            return '%' + c.charCodeAt(0).toString(16);
        });
    };
    MWSClientBase.prototype.createQueryString = function (params) {
        var query_parts = [];
        var keys = Object.keys(params);
        for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
            var k = keys_1[_i];
            query_parts.push(k + '=' + this.urlEncode(params[k]));
        }
        var queryString = query_parts.join('&');
        return queryString;
    };
    MWSClientBase.prototype.calcSignature = function (endpoint, path, params) {
        var str = '';
        var keys = Object.keys(params);
        // sort by key
        keys = keys.sort();
        for (var i = 0; i < keys.length; i++) {
            if (i != 0)
                str += '&';
            str += keys[i] + '=' + this.urlEncode(params[keys[i]]);
        }
        var stringToSign = 'POST\n' + endpoint + '\n' + path + '\n' + str;
        var hmac = this.calcHMAC(stringToSign, this.access_secret);
        return hmac;
    };
    MWSClientBase.prototype.callApi = function () {
        var _args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            _args[_i] = arguments[_i];
        }
        var section = null;
        var version = null;
        var action = null;
        var params = {};
        var feed = null;
        var cbk = null;
        var argc = arguments.length;
        if (argc < 4) {
            throw new Error('Invalid paramters, at least 4 parameter needed!');
        }
        section = arguments[0];
        version = arguments[1];
        action = arguments[2];
        if (argc == 4) {
            cbk = arguments[3];
        }
        else if (argc == 5) {
            params = arguments[3];
            cbk = arguments[4];
        }
        else {
            params = arguments[3];
            feed = arguments[4];
            cbk = arguments[5];
        }
        var headers = {
            'User-Agent': this.getUserAgent(),
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        };
        var requiredParams = {
            AWSAccessKeyId: this.access_key,
            Action: action,
            SellerId: this.seller_id,
            SignatureMethod: 'HmacSHA256',
            SignatureVersion: '2',
            Timestamp: new Date().toISOString(),
            Version: version
        };
        // merge api specific parameters supplied by user
        // tslint:disable-next-line: forin
        for (var k in params) {
            requiredParams[k] = params[k];
        }
        if (this.auth_token) {
            requiredParams['MWSAuthToken'] = this.auth_token;
        }
        if (action == 'SubmitFeed') {
            if (params['FeedType'].indexOf('FLAT_FILE') > -1) {
                // flat file feed
                if (this.marketplace == 'CN') {
                    headers['Content-Type'] = 'text/tab-separated-values; charset=utf-8';
                    feed = this.encodeContent(feed, 'utf8');
                }
                else if (this.marketplace == 'JP') {
                    headers['Content-Type'] = 'text/tab-separated-values; charset=Shift_JIS';
                    feed = this.encodeContent(feed, 'shift_jis');
                }
                else {
                    headers['Content-Type'] = 'text/tab-separated-values; charset=iso-8859-1';
                    feed = this.encodeContent(feed, 'iso-8859-1');
                }
            }
            else {
                headers['Content-Type'] = 'text/xml';
                feed = this.encodeContent(feed, 'utf8');
            }
            requiredParams['ContentMD5Value'] = this.calcMD5(feed);
        }
        if (requiredParams['ReportType'] && (requiredParams['ReportType'] === '_GET_GST_MTR_B2C_CUSTOM_' || requiredParams['ReportType'] === '_GET_GST_MTR_B2B_CUSTOM_')) {
            requiredParams['MarketplaceIdList.Id.1'] = this.marketplace_id;
        }
        requiredParams['Signature'] = this.calcSignature(this.endpoint, '/' + section + '/' + version, requiredParams);
        var queryString = this.createQueryString(requiredParams);
        var url = this.protocol + '://' + this.endpoint + '/' + section + '/' + version;
        if (queryString) {
            url = url + '?' + queryString;
        }
        if (action == 'SubmitFeed') {
            return this.makeHttpRequest('POST', url, headers, feed, cbk);
        }
        else {
            return this.makeHttpRequest('POST', url, headers, null, cbk);
        }
    };
    MWSClientBase.prototype.callApiFormData = function () {
        var _args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            _args[_i] = arguments[_i];
        }
        var section = null;
        var version = null;
        var action = null;
        var params = {};
        // let feed = null
        var cbk = null;
        var argc = arguments.length;
        if (argc < 4) {
            throw new Error('Invalid paramters, at least 4 parameter needed!');
        }
        section = arguments[0];
        version = arguments[1];
        action = arguments[2];
        if (argc == 4) {
            cbk = arguments[3];
        }
        else if (argc == 5) {
            params = arguments[3];
            cbk = arguments[4];
        }
        else {
            params = arguments[3];
            // feed = arguments[4]
            cbk = arguments[5];
        }
        var headers = {
            Host: 'mws.amazonservices.in',
            'User-Agent': '@ericblade/mws-simple/5.1.1 (Language=Javascript)',
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        };
        var requiredParams = {
            AWSAccessKeyId: this.access_key,
            Action: action,
            SellerId: this.seller_id,
            SignatureMethod: 'HmacSHA256',
            SignatureVersion: '2',
            Timestamp: new Date().toISOString(),
            responseFormat: 'xml',
            MarketplaceId: this.marketplace_id,
            Version: version
        };
        // merge api specific parameters supplied by user
        // tslint:disable-next-line: forin
        for (var k in params) {
            requiredParams[k] = params[k];
        }
        if (this.auth_token) {
            requiredParams['MWSAuthToken'] = this.auth_token;
        }
        requiredParams['Signature'] = this.calcSignature(this.endpoint, '/' + section + '/' + version, requiredParams);
        var url = this.protocol + '://' + this.endpoint + '/' + section + '/' + version;
        // console.log(params)
        // console.log(this.createQueryString(requiredParams))
        return this.makeHttpRequestFormData(url, headers, requiredParams, cbk);
    };
    // A generic api call proxy
    MWSClientBase.prototype._invokeApi = function (section, name) {
        var params = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            params[_i - 2] = arguments[_i];
        }
        if (!api_mapping[section]) {
            throw new Error("Invalid MWS API Section name: " + section + "!");
        }
        return this.callApi.apply(this, __spreadArrays([section, api_mapping[section].version, name], params));
    };
    // A generaic api formData call proxy
    MWSClientBase.prototype._invokeApiFormData = function (section, name) {
        var params = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            params[_i - 2] = arguments[_i];
        }
        if (!api_mapping[section]) {
            throw new Error("Invalid MWS API Section name: " + section + "!");
        }
        return this.callApiFormData.apply(this, __spreadArrays([section, api_mapping[section].version, name], params));
    };
    // Feeds API Methods
    MWSClientBase.prototype.SubmitFeed = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Feeds', 'SubmitFeed'], params));
    };
    MWSClientBase.prototype.GetFeedSubmissionList = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Feeds', 'GetFeedSubmissionList'], params));
    };
    MWSClientBase.prototype.GetFeedSubmissionListByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Feeds', 'GetFeedSubmissionListByNextToken'], params));
    };
    MWSClientBase.prototype.GetFeedSubmissionCount = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Feeds', 'GetFeedSubmissionCount'], params));
    };
    MWSClientBase.prototype.CancelFeedSubmissions = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Feeds', 'CancelFeedSubmissions'], params));
    };
    MWSClientBase.prototype.GetFeedSubmissionResult = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Feeds', 'GetFeedSubmissionResult'], params));
    };
    // Finance API Methods
    MWSClientBase.prototype.ListFinancialEventGroups = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Finances', 'ListFinancialEventGroups'], params));
    };
    MWSClientBase.prototype.ListFinancialEventGroupsByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Finances', 'ListFinancialEventGroupsByNextToken'], params));
    };
    MWSClientBase.prototype.ListFinancialEvents = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Finances', 'ListFinancialEvents'], params));
    };
    MWSClientBase.prototype.ListFinancialEventsByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Finances', 'ListFinancialEventsByNextToken'], params));
    };
    // Fulfillment Inbound Shipment API Methods
    MWSClientBase.prototype.GetInboundGuidanceForSKU = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetInboundGuidanceForSKU'], params));
    };
    MWSClientBase.prototype.GetInboundGuidanceForASIN = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetInboundGuidanceForASIN'], params));
    };
    MWSClientBase.prototype.CreateInboundShipmentPlan = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'CreateInboundShipmentPlan'], params));
    };
    MWSClientBase.prototype.CreateInboundShipment = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'CreateInboundShipment'], params));
    };
    MWSClientBase.prototype.UpdateInboundShipment = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'UpdateInboundShipment'], params));
    };
    MWSClientBase.prototype.GetPreorderInfo = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetPreorderInfo'], params));
    };
    MWSClientBase.prototype.ConfirmPreorder = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'ConfirmPreorder'], params));
    };
    MWSClientBase.prototype.GetPrepInstructionsForSKU = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetPrepInstructionsForSKU'], params));
    };
    MWSClientBase.prototype.GetPrepInstructionsForASIN = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetPrepInstructionsForASIN'], params));
    };
    MWSClientBase.prototype.PutTransportContent = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'PutTransportContent'], params));
    };
    MWSClientBase.prototype.EstimateTransportRequest = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'EstimateTransportRequest'], params));
    };
    MWSClientBase.prototype.GetTransportContent = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetTransportContent'], params));
    };
    MWSClientBase.prototype.ConfirmTransportRequest = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'ConfirmTransportRequest'], params));
    };
    MWSClientBase.prototype.VoidTransportRequest = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'VoidTransportRequest'], params));
    };
    MWSClientBase.prototype.GetPackageLabels = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetPackageLabels'], params));
    };
    MWSClientBase.prototype.GetUniquePackageLabels = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetUniquePackageLabels'], params));
    };
    MWSClientBase.prototype.GetPalletLabels = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetPalletLabels'], params));
    };
    MWSClientBase.prototype.GetBillOfLading = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'GetBillOfLading'], params));
    };
    MWSClientBase.prototype.ListInboundShipments = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'ListInboundShipments'], params));
    };
    MWSClientBase.prototype.ListInboundShipmentsByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'ListInboundShipmentsByNextToken'], params));
    };
    MWSClientBase.prototype.ListInboundShipmentItems = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'ListInboundShipmentItems'], params));
    };
    MWSClientBase.prototype.ListInboundShipmentItemsByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInboundShipment', 'ListInboundShipmentItemsByNextToken'], params));
    };
    // Fulfillment Inventory API Methods
    MWSClientBase.prototype.ListInventorySupply = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInventory', 'ListInventorySupply'], params));
    };
    MWSClientBase.prototype.ListInventorySupplyByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentInventory', 'ListInventorySupplyByNextToken'], params));
    };
    // Fulfillment Outbound Shipment
    MWSClientBase.prototype.GetFulfillmentPreview = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'GetFulfillmentPreview'], params));
    };
    MWSClientBase.prototype.CreateFulfillmentOrder = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'CreateFulfillmentOrder'], params));
    };
    MWSClientBase.prototype.UpdateFulfillmentOrder = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'UpdateFulfillmentOrder'], params));
    };
    MWSClientBase.prototype.GetFulfillmentOrder = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'GetFulfillmentOrder'], params));
    };
    MWSClientBase.prototype.ListAllFulfillmentOrders = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'ListAllFulfillmentOrders'], params));
    };
    MWSClientBase.prototype.ListAllFulfillmentOrdersByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'ListAllFulfillmentOrdersByNextToken'], params));
    };
    MWSClientBase.prototype.GetPackageTrackingDetails = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'GetPackageTrackingDetails'], params));
    };
    MWSClientBase.prototype.CancelFulfillmentOrder = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'CancelFulfillmentOrder'], params));
    };
    MWSClientBase.prototype.ListReturnReasonCodes = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'ListReturnReasonCodes'], params));
    };
    MWSClientBase.prototype.CreateFulfillmentReturn = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'CreateFulfillmentReturn'], params));
    };
    // Merchant Fulfillment API Methods
    MWSClientBase.prototype.GetEligibleShippingServices = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['MerchantFulfillment', 'GetEligibleShippingServices'], params));
    };
    MWSClientBase.prototype.CreateShipment = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['MerchantFulfillment', 'CreateShipment'], params));
    };
    MWSClientBase.prototype.GetShipment = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'GetShipment'], params));
    };
    MWSClientBase.prototype.CancelShipment = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['FulfillmentOutboundShipment', 'CancelShipment'], params));
    };
    // Orders API Methods
    MWSClientBase.prototype.ListOrders = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Orders', 'ListOrders'], params));
    };
    MWSClientBase.prototype.ListOrdersByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Orders', 'ListOrdersByNextToken'], params));
    };
    MWSClientBase.prototype.GetOrder = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Orders', 'GetOrder'], params));
    };
    MWSClientBase.prototype.ListOrderItems = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Orders', 'ListOrderItems'], params));
    };
    MWSClientBase.prototype.ListOrderItemsByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Orders', 'ListOrderItemsByNextToken'], params));
    };
    // Products API Methods
    MWSClientBase.prototype.ListMatchingProducts = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'ListMatchingProducts'], params));
    };
    MWSClientBase.prototype.GetMatchingProduct = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetMatchingProduct'], params));
    };
    MWSClientBase.prototype.GetMatchingProductForId = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetMatchingProductForId'], params));
    };
    MWSClientBase.prototype.GetCompetitivePricingForSKU = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetCompetitivePricingForSKU'], params));
    };
    MWSClientBase.prototype.GetCompetitivePricingForASIN = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetCompetitivePricingForASIN'], params));
    };
    MWSClientBase.prototype.GetLowestOfferListingsForSKU = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetLowestOfferListingsForSKU'], params));
    };
    MWSClientBase.prototype.GetLowestOfferListingsForASIN = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetLowestOfferListingsForASIN'], params));
    };
    MWSClientBase.prototype.GetLowestPricedOffersForSKU = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetLowestPricedOffersForSKU'], params));
    };
    MWSClientBase.prototype.GetLowestPricedOffersForASIN = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        // console.log('INVOKINGG');
        return this._invokeApiFormData.apply(this, __spreadArrays(['Products', 'GetLowestPricedOffersForASIN'], params));
        // return [];
        // return this._invokeApi('Products', 'GetLowestPricedOffersForASIN', ...params)
    };
    MWSClientBase.prototype.GetMyFeesEstimate = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetMyFeesEstimate'], params));
    };
    MWSClientBase.prototype.GetMyPriceForSKU = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetMyPriceForSKU'], params));
    };
    MWSClientBase.prototype.GetMyPriceForASIN = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetMyPriceForASIN'], params));
    };
    MWSClientBase.prototype.GetProductCategoriesForSKU = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetProductCategoriesForSKU'], params));
    };
    MWSClientBase.prototype.GetProductCategoriesForASIN = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Products', 'GetProductCategoriesForASIN'], params));
    };
    // Recommendations API Methods
    MWSClientBase.prototype.GetLastUpdatedTimeForRecommendations = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Recommendations', 'GetLastUpdatedTimeForRecommendations'], params));
    };
    MWSClientBase.prototype.ListRecommendations = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Recommendations', 'ListRecommendations'], params));
    };
    MWSClientBase.prototype.ListRecommendationsByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Recommendations', 'ListRecommendationsByNextToken'], params));
    };
    // Reports API Methods
    MWSClientBase.prototype.RequestReport = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'RequestReport'], params));
    };
    MWSClientBase.prototype.GetReportRequestList = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReportRequestList'], params));
    };
    MWSClientBase.prototype.GetReportRequestListByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReportRequestListByNextToken'], params));
    };
    MWSClientBase.prototype.GetReportRequestCount = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReportRequestCount'], params));
    };
    MWSClientBase.prototype.CancelReportRequests = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'CancelReportRequests'], params));
    };
    MWSClientBase.prototype.GetReportList = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReportList'], params));
    };
    MWSClientBase.prototype.GetReportListByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReportListByNextToken'], params));
    };
    MWSClientBase.prototype.GetReportCount = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReportCount'], params));
    };
    MWSClientBase.prototype.GetReport = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReport'], params));
    };
    MWSClientBase.prototype.ManageReportSchedule = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'ManageReportSchedule'], params));
    };
    MWSClientBase.prototype.GetReportScheduleList = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReportScheduleList'], params));
    };
    MWSClientBase.prototype.GetReportScheduleListByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReportScheduleListByNextToken'], params));
    };
    MWSClientBase.prototype.GetReportScheduleCount = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'GetReportScheduleCount'], params));
    };
    MWSClientBase.prototype.UpdateReportAcknowledgements = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Reports', 'UpdateReportAcknowledgements'], params));
    };
    // Sellers API Methods
    MWSClientBase.prototype.ListMarketplaceParticipations = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Sellers', 'ListMarketplaceParticipations'], params));
    };
    MWSClientBase.prototype.ListMarketplaceParticipationsByNextToken = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Sellers', 'ListMarketplaceParticipationsByNextToken'], params));
    };
    // Subscription API Methods
    MWSClientBase.prototype.RegisterDestination = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Subscriptions', 'RegisterDestination'], params));
    };
    MWSClientBase.prototype.DeregisterDestination = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Subscriptions', 'DeregisterDestination'], params));
    };
    MWSClientBase.prototype.ListRegisteredDestinations = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Subscriptions', 'ListRegisteredDestinations'], params));
    };
    MWSClientBase.prototype.SendTestNotificationToDestination = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Subscriptions', 'SendTestNotificationToDestination'], params));
    };
    MWSClientBase.prototype.CreateSubscription = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Subscriptions', 'CreateSubscription'], params));
    };
    MWSClientBase.prototype.GetSubscription = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Subscriptions', 'GetSubscription'], params));
    };
    MWSClientBase.prototype.DeleteSubscription = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Subscriptions', 'DeleteSubscription'], params));
    };
    MWSClientBase.prototype.ListSubscriptions = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Subscriptions', 'ListSubscriptions'], params));
    };
    MWSClientBase.prototype.UpdateSubscription = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['Subscriptions', 'UpdateSubscription'], params));
    };
    // EasyShip API Methods
    MWSClientBase.prototype.ListPickupSlots = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['EasyShip', 'ListPickupSlots'], params));
    };
    MWSClientBase.prototype.CreateScheduledPackage = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['EasyShip', 'CreateScheduledPackage'], params));
    };
    MWSClientBase.prototype.UpdateScheduledPackages = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['EasyShip', 'UpdateScheduledPackages'], params));
    };
    MWSClientBase.prototype.GetScheduledPackage = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays(['EasyShip', 'GetScheduledPackage'], params));
    };
    // Get API Service Status Methods
    MWSClientBase.prototype.GetServiceStatus = function (section) {
        var params = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            params[_i - 1] = arguments[_i];
        }
        return this._invokeApi.apply(this, __spreadArrays([section, 'GetServiceStatus'], params));
    };
    return MWSClientBase;
}());
exports.MWSClientBase = MWSClientBase;
