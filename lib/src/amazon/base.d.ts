declare abstract class MWSClientBase {
  marketplace_info: any
  useragent: string
  marketplace: string | null
  marketplace_id: string | null
  protocol: string | null
  endpoint: string | null
  site_url: string | null
  access_key: string | null
  access_secret: string | null
  seller_id: string | null
  auth_token: string | null
  static version(): string
  static escapeUserAgentFieldValue(v: string): string
  static getMarketplaceId(country_code: string): string
  /**
   * Return null when response code is 20x, for others, return an error object with all returned data inside it
   */
  static parseResponseError(
    status: any,
    headers: any,
    body: any
  ): {
    status: any
    headers: any
    quota: {}
    body: any
  }
  static parseResponse(
    status: any,
    headers: any,
    body: any
  ): {
    status: any
    headers: any
    body: any
    quota: {}
  }
  getMarketplaceInfo(country_code: string): any
  urlEncode(str: string): string
  createQueryString(params: any): string
  abstract calcMD5(content: string): string
  abstract calcHMAC(content: string, secret: string): string
  abstract makeHttpRequest(method: any, url: any, headers: any, body: any, cbk: any): void
  abstract makeHttpRequestFormData(url: any, headers: any, formData: any, cbk: any): void
  abstract getUserAgent(): string
  abstract encodeContent(content: string, encoding: string): any
  calcSignature(endpoint: any, path: any, params: any): string
  constructor(marketplace: any, access_key: any, access_secret: any, seller_id: any, auth_token?: string)
  callApi(..._args: Array<any>): void
  callApiFormData(..._args: Array<any>): void
  _invokeApi(section: any, name: any, ...params: any[]): void
  _invokeApiFormData(section: any, name: any, ...params: any[]): void
  SubmitFeed(...params: any[]): void
  GetFeedSubmissionList(...params: any[]): void
  GetFeedSubmissionListByNextToken(...params: any[]): void
  GetFeedSubmissionCount(...params: any[]): void
  CancelFeedSubmissions(...params: any[]): void
  GetFeedSubmissionResult(...params: any[]): void
  ListFinancialEventGroups(...params: any[]): void
  ListFinancialEventGroupsByNextToken(...params: any[]): void
  ListFinancialEvents(...params: any[]): void
  ListFinancialEventsByNextToken(...params: any[]): void
  GetInboundGuidanceForSKU(...params: any[]): void
  GetInboundGuidanceForASIN(...params: any[]): void
  CreateInboundShipmentPlan(...params: any[]): void
  CreateInboundShipment(...params: any[]): void
  UpdateInboundShipment(...params: any[]): void
  GetPreorderInfo(...params: any[]): void
  ConfirmPreorder(...params: any[]): void
  GetPrepInstructionsForSKU(...params: any[]): void
  GetPrepInstructionsForASIN(...params: any[]): void
  PutTransportContent(...params: any[]): void
  EstimateTransportRequest(...params: any[]): void
  GetTransportContent(...params: any[]): void
  ConfirmTransportRequest(...params: any[]): void
  VoidTransportRequest(...params: any[]): void
  GetPackageLabels(...params: any[]): void
  GetUniquePackageLabels(...params: any[]): void
  GetPalletLabels(...params: any[]): void
  GetBillOfLading(...params: any[]): void
  ListInboundShipments(...params: any[]): void
  ListInboundShipmentsByNextToken(...params: any[]): void
  ListInboundShipmentItems(...params: any[]): void
  ListInboundShipmentItemsByNextToken(...params: any[]): void
  ListInventorySupply(...params: any[]): void
  ListInventorySupplyByNextToken(...params: any[]): void
  GetFulfillmentPreview(...params: any[]): void
  CreateFulfillmentOrder(...params: any[]): void
  UpdateFulfillmentOrder(...params: any[]): void
  GetFulfillmentOrder(...params: any[]): void
  ListAllFulfillmentOrders(...params: any[]): void
  ListAllFulfillmentOrdersByNextToken(...params: any[]): void
  GetPackageTrackingDetails(...params: any[]): void
  CancelFulfillmentOrder(...params: any[]): void
  ListReturnReasonCodes(...params: any[]): void
  CreateFulfillmentReturn(...params: any[]): void
  GetEligibleShippingServices(...params: any[]): void
  CreateShipment(...params: any[]): void
  GetShipment(...params: any[]): void
  CancelShipment(...params: any[]): void
  ListOrders(...params: any[]): void
  ListOrdersByNextToken(...params: any[]): void
  GetOrder(...params: any[]): void
  ListOrderItems(...params: any[]): void
  ListOrderItemsByNextToken(...params: any[]): void
  ListMatchingProducts(...params: any[]): void
  GetMatchingProduct(...params: any[]): void
  GetMatchingProductForId(...params: any[]): void
  GetCompetitivePricingForSKU(...params: any[]): void
  GetCompetitivePricingForASIN(...params: any[]): void
  GetLowestOfferListingsForSKU(...params: any[]): void
  GetLowestOfferListingsForASIN(...params: any[]): void
  GetLowestPricedOffersForSKU(...params: any[]): void
  GetLowestPricedOffersForASIN(...params: any[]): void
  GetMyFeesEstimate(...params: any[]): void
  GetMyPriceForSKU(...params: any[]): void
  GetMyPriceForASIN(...params: any[]): void
  GetProductCategoriesForSKU(...params: any[]): void
  GetProductCategoriesForASIN(...params: any[]): void
  GetLastUpdatedTimeForRecommendations(...params: any[]): void
  ListRecommendations(...params: any[]): void
  ListRecommendationsByNextToken(...params: any[]): void
  RequestReport(...params: any[]): void
  GetReportRequestList(...params: any[]): void
  GetReportRequestListByNextToken(...params: any[]): void
  GetReportRequestCount(...params: any[]): void
  CancelReportRequests(...params: any[]): void
  GetReportList(...params: any[]): void
  GetReportListByNextToken(...params: any[]): void
  GetReportCount(...params: any[]): void
  GetReport(...params: any[]): void
  ManageReportSchedule(...params: any[]): void
  GetReportScheduleList(...params: any[]): void
  GetReportScheduleListByNextToken(...params: any[]): void
  GetReportScheduleCount(...params: any[]): void
  UpdateReportAcknowledgements(...params: any[]): void
  ListMarketplaceParticipations(...params: any[]): void
  ListMarketplaceParticipationsByNextToken(...params: any[]): void
  RegisterDestination(...params: any[]): void
  DeregisterDestination(...params: any[]): void
  ListRegisteredDestinations(...params: any[]): void
  SendTestNotificationToDestination(...params: any[]): void
  CreateSubscription(...params: any[]): void
  GetSubscription(...params: any[]): void
  DeleteSubscription(...params: any[]): void
  ListSubscriptions(...params: any[]): void
  UpdateSubscription(...params: any[]): void
  ListPickupSlots(...params: any[]): void
  CreateScheduledPackage(...params: any[]): void
  UpdateScheduledPackages(...params: any[]): void
  GetScheduledPackage(...params: any[]): void
  GetServiceStatus(section: any, ...params: any[]): void
}
export { MWSClientBase }
